<?php

get_header();
$news = ProductsService::getNews(1,3);
?>

    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/single.css"/>


<?php get_header();?>
<?php while ( have_posts() ) : the_post(); ?>
    <div class="body">
        <div class="container mb-5">
            <div class="clear"></div>
            <div class="main">
                <div class="row mt-5 mb-5 single-container">
                    <div class="col-8"><?php the_content(); ?></div>
                    <div class="col-4">
                        <div class="box">
	                        <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                <p class="mb-1">Share</p>
	                        <?php } else { ?>
                                <p class="mb-1">Udostępnij</p>
	                        <?php } ?>
                            <hr>
                            <div class="box-logos">
                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank">
                                    <img class="box-logo" src="<?php bloginfo('template_directory'); ?>/assets/img/social-facebook@2.png">
                                </a>
                                <a href="https://twitter.com/intent/tweet?url=<?php echo get_permalink(); ?>">
                                <img class="box-logo" src="<?php bloginfo('template_directory'); ?>/assets/img/social-linkedin@2.png">
                                </a>
                                <a href="https://www.linkedin.com/sharing/share-offsite/?url=<?php echo get_permalink(); ?>" target="_blank">
                                <img class="box-logo" src="<?php bloginfo('template_directory'); ?>/assets/img/social-twitter@2.png">
                                </a>
                            </div>

                        </div>
                        <div class="recommended mt-4">
	                        <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                <p class="title">Related posts: </p>
	                        <?php } else { ?>
                                <p class="title">Warto przeczytać: </p>
	                        <?php } ?>
                            <?php while ( $news->have_posts() ) : $news->the_post(); ?>
                            <a style="color: black; text-decoration: none" href="<?php the_permalink();?>">
                                <p class="" style="font-weight: 700;"><?php the_title(); ?></p>
                                <small><?php the_excerpt($post); ?></small>
                                <?php endwhile; ?>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="clear"></div>
        </div>
    </div>
<?php endwhile; ?>

<?php get_footer();
