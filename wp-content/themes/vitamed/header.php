<!DOCTYPE html>
<html lang="pl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vitamed</title>
    <style>
        main {
            cursor: url(<?php bloginfo('template_directory'); ?>/assets/img/cursor.png), auto;
        }
    </style>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

    <?php
    $header = new HeaderService($wp_query);
    if (DefaultHelper::checkEn() == 'en') {
        $products = ProductsService::getProducts(null, -1, 'header');
    } else {
        $products = ProductsService::getProducts(null, -1, 'Produkty');
    }
    ?>
    <script src="https://unpkg.com/scrollreveal"></script>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/app.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/animate.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/style.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/header.css">
    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
    <?php if (is_front_page() || is_home()) {  ?>
        <style>
            @media (max-width: 1024px) {
                .title-container {
                    margin-top: 20rem;
                    align-items: flex-start;
                }
            }

            @media (max-width: 768px) {
                .title-container {
                    margin-top: 11rem;
                    text-align: center;
                }
            }

            @media (max-width: 480px) {
                .title-container {
                    margin-top: 11rem;
                    text-align: center;
                }
            }
        </style>
    <?php } else { ?>
        <style>
            @media (max-width: 1024px) {
                .title-container {
                    margin-top: 20rem;
                    align-items: flex-start;
                }
            }

            @media (max-width: 768px) {
                .title-container {
                    margin-top: 13rem;
                    text-align: center;
                }
            }

            @media (max-width: 480px) {
                .title-container {
                    margin-top: 14rem;
                    text-align: center;
                }
            }
        </style>
    <?php } ?>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/flickity.js"></script>
    <script>
        $('.pykk').flickity({
            arrowShape: 'M 12.881 47.156 L 41.738 20.164 L 48.024 26.125 L 32.959 41.407 L 84.983 41.407 L 84.983 52.571 L 32.923 52.636 L 47.184 65.096 L 42.005 72.977 Z'
        });
    </script>
    <Style>
        .flickity-button-icon {
            -webkit-box-shadow: none !important;
            -moz-box-shadow: none !important;
            box-shadow: none !important;
            border-radius: 50%;
        }
    </Style>
</head>

<body data-zoom="0.67">

    <header class="header relative" style="height: <?php echo $header->getHeaderHeight(); ?>">
        <div class="header-image" style="background-image: url(<?php echo $header->getHeaderImg(); ?>); background-color: #edebf6; ">
            <script>
                function browserTester(browserString) {
                    return navigator.userAgent.toLowerCase().indexOf(browserString) > -1;
                }

                if (window.devicePixelRatio == 1.5 && window.innerWidth > 764) {
                    // $('.header-container').css('transform', 'scale(1.1)');
                    $('.header-logo').css('margin-left', '8rem');
                    if (browserTester('chrome')) {
                        $('body').css({
                            "zoom": "67%",
                            "-ms-zoom": "67%"
                        });

                    } else if (browserTester('mozilla')) {
                        $('body').css({
                            "transform": "scale(0.67)",
                            "transform-origin": "0 0",
                            "position": "absolute"
                        });

                        $('body').css("width", "150vw");
                        $('html').css("height", "100vh");
                    }
                }
            </script>
            <?php require 'page-templates/misc/preloader.php'; ?>

            <div class="spinner-wrapper">
                <div class="spinner">
                    <img src="https://vitamed.weblider24.pl/wp-content/uploads/2021/03/preloaderimg.png" />
                </div>
            </div>
            <div class="header-mask" style="height: <?php echo $header->getMaskHeight(); ?>; background-image: url(<?php echo $header->getMaskImg(); ?>)">
                <div class="header-container <?php echo $header->getClass()['header']; ?>">
                    <?php if (DefaultHelper::checkEn() == 'en') { ?>
                        <a href="<?php echo site_url("/en/"); ?>">
                        <?php } else { ?>
                            <a href="<?php echo site_url("/"); ?>">
                            <?php } ?>
                            <img src="<?php echo $header->getLogo(); ?>" class="header-logo">
                            </a>
                            <div class="header-nav-container">
                                <!--                        ANGIELSKIEEEEEEE       -->
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    <nav class="header-nav">
                                        <div class="upper-nav">
                                            <div class="doctor-container">
                                                <?php $linkiEN = wp_get_nav_menu_items("LINKI EN");  ?>
                                                <img class="upper-icon doctor" src="<?php echo $header->getIconLekarz(); ?>">
                                                <a href="<?php echo $linkiEN[0]->url ?>" target="_blank" rel="noopener" class="upper-link">
                                                    <p class="upper-text <?php echo $header->getClass()['upper-text']; ?>"><?php echo $linkiEN[0]->title ?></p>
                                                </a>
                                            </div>
                                            <div class="pharm-container">
                                                <img class="upper-icon pharmacist" src="<?php echo $header->getIconFarmaceuta(); ?>">
                                                <a href="<?php echo $linkiEN[1]->title ?>" target="_blank" rel="noopener" class="upper-link">
                                                    <p class="upper-text <?php echo $header->getClass()['upper-text']; ?>"><?php echo $linkiEN[1]->title ?></p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="language-nav">
                                            <ul class="language-list">
                                                <li class="language-item polish">
                                                    <a href=" <?php echo apply_filters('wpml_permalink', get_the_permalink(), 'pl'); ?>" class="language-link">
                                                        <p class="language-text <?php echo $header->getClass()['language']; ?> ">PL</p>
                                                    </a>
                                                </li>
                                                <li class="language-item english">
                                                    <a href="<?php echo apply_filters('wpml_permalink', get_the_permalink(), 'en'); ?>" class="language-link">
                                                        <p class="language-text <?php echo $header->getClass()['language']; ?>">EN</p>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="bottom-nav">
                                            <ul class="bottom-menu">
                                                <?php $menuPL =  wp_get_nav_menu_items("MENU EN"); ?>
                                                <?php foreach ($menuPL as $mpl) { ?>
                                                    <li class="bottom-item">
                                                        <a class="bottom-link <?php echo $header->getClass()['bottom-link']; ?>" href="<?php echo $mpl->url; ?>"><?php echo $mpl->title; ?></a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </nav>
                                <?php } else { ?>

                                    <!--                            POLSKIEIEEEEEEEEEEEEEE-->

                                    <nav class="header-nav">
                                        <div class="upper-nav">
                                            <?php $linkiPL =  wp_get_nav_menu_items("LINKI PL"); ?>
                                            <!-- <?php foreach ($linkiPL as $lpl) { ?>
                                                <li class="bottom-item">
                                                    <a class="bottom-link <?php echo $header->getClass()['bottom-link']; ?>" href="<?php echo $mpl->url; ?>"><?php echo $mpl->title; ?></a>
                                                </li>
                                            <?php } ?> -->
                                            <div class="doctor-container">
                                                <img class="upper-icon doctor" src="<?php echo $header->getIconLekarz(); ?>">
                                                <a href="<?php echo $linkiPL[0]->url ?>" target="_blank" rel="noopener" class="upper-link">
                                                    <p class="upper-text <?php echo $header->getClass()['upper-text']; ?>"><?php echo $linkiPL[0]->title ?></p>
                                                </a>
                                            </div>
                                            <div class="pharm-container">
                                                <img class="upper-icon pharmacist" src="<?php echo $header->getIconFarmaceuta(); ?>">
                                                <a href="<?php echo $linkiPL[1]->url ?>" target="_blank" rel="noopener" class="upper-link">
                                                    <p class="upper-text <?php echo $header->getClass()['upper-text']; ?>">
                                                        <?php echo $linkiPL[1]->title ?>
                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="language-nav">
                                            <ul class="language-list">
                                                <li class="language-item polish">
                                                    <a href=" <?php echo apply_filters('wpml_permalink', get_the_permalink(), 'pl'); ?>" class="language-link">
                                                        <p class="language-text <?php echo $header->getClass()['language']; ?> ">PL</p>
                                                    </a>
                                                </li>
                                                <li class="language-item english">
                                                    <a href="<?php echo apply_filters('wpml_permalink', get_the_permalink(), 'en'); ?>" class="language-link">
                                                        <p class="language-text <?php echo $header->getClass()['language']; ?>">EN</p>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="bottom-nav">
                                            <ul class="bottom-menu">
                                                <?php $menuPL =  wp_get_nav_menu_items("MENU PL"); ?>
                                                <!--                                                --><?php //var_dump($menuPL); 
                                                                                                        ?>
                                                <?php foreach ($menuPL as $mpl) { ?>
                                                    <li class="bottom-item">
                                                        <a class="bottom-link <?php echo $header->getClass()['bottom-link']; ?>" href="<?php echo $mpl->url; ?>"><?php echo $mpl->title; ?></a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </nav>

                                <?php } ?>
                                <div class="search-container">
                                    <div class="search-input">
                                        <form action="/wyszukiwarka" method="POST">
                                            <input name="slug" class="search-input-text" type="text">
                                            <button type="submit" class="search-button">Szukaj</button>
                                        </form>
                                    </div>


                                    <img src="<?php bloginfo('template_directory') ?>/assets/img/ikona-szukaj@2.png" class="search-icon">
                                </div>
                            </div>

                            <div class="mobile-container">
                                <div class="mobile-icon"></div>
                                <div class="mobile-block">
                                    <ul class="mobile-nav">
                                        <?php
                                        if (DefaultHelper::checkEn() == 'en') { ?>
                                            <?php foreach ($menuEN as $men) { ?>
                                                <li class="mobile-item">
                                                    <a class="mobile-link" href="<?php echo $men->url; ?>"><?php echo $men->title; ?></a>
                                                </li>
                                            <?php } ?>
                                        <?php  } else { ?>
                                            <?php foreach ($menuPL as $mpl) { ?>
                                                <li class="mobile-item">
                                                    <a class="mobile-link" href="<?php echo $mpl->url; ?>"><?php echo $mpl->title; ?></a>
                                                </li>
                                            <?php } ?>
                                        <?php  }  ?>
                                    </ul>

                                    <div class="redirect-links">
                                        <li class="mobile-item" style="list-style: none;">
                                            <img class="upper-icon-mobile" src="/wp-content/themes/vitamed/assets/img/ikona-dla-lekarzy-granatowa@2.png">
                                            <?php
                                            if (DefaultHelper::checkEn() == 'en') { ?>
                                                <a class="mobile-link" target="_blank" href="<?php echo $linkiEN[0]->url ?>"><?php echo $linkiEN[0]->title ?></a>
                                            <?php  } else { ?>
                                                <a class="mobile-link" target="_blank" href="<?php echo $linkiPL[0]->url ?>"><?php echo $linkiPL[0]->title ?></a>
                                            <?php  }  ?>
                                        </li>
                                        <li class="mobile-item" style="list-style: none;">
                                            <img class="upper-icon-mobile" src="/wp-content/themes/vitamed/assets/img/ikona-dla-farmaceutow-granatowa@2.png">
                                            <?php
                                            if (DefaultHelper::checkEn() == 'en') { ?>
                                                <a class="mobile-link" target="_blank" href="<?php echo $linkiEN[1]->url ?> "><?php echo $linkiEN[1]->title ?></a>
                                            <?php  } else { ?>
                                                <a class="mobile-link" target="_blank" href="<?php echo $linkiPL[1]->url ?>"><?php echo $linkiPL[1]->title ?></a>
                                            <?php  }  ?>
                                        </li>
                                        <div class="flags-mobile">
                                            <a class="flag" href="<?php echo apply_filters('wpml_permalink', get_the_permalink(), 'pl'); ?>">
                                                <p>PL</p>
                                            </a>
                                            <a class="flag" href="<?php echo apply_filters('wpml_permalink', get_the_permalink(), 'en'); ?>">
                                                <p>EN</p>
                                            </a>

                                        </div>
                                    </div>

                                    </ul>
                                    <!-- <div class="doctor-container-mobile">
                                    <img class="upper-icon" src="<?php echo $header->getIconLekarz(); ?>">
                                    <a href="#" class="upper-link">
                                        <p class="upper-text">Dla lekarzy</p>
                                    </a>
                                </div>
                                <div class="pharm-container-mobile">
                                    <img class="upper-icon" src="<?php echo $header->getIconFarmaceuta(); ?>">
                                    <a href="#" class="upper-link">
                                        <p class="upper-text">Dla farmaceutów</p>
                                    </a>
                                </div> -->
                                </div>
                            </div>

                </div>

                <div class="header-title-container col-12 <?php if (!($header->isProduct())) {  ?> container <?php } ?>" <?php if ($header->isProduct()) {  ?> style="justify-content: space-around" <?php } ?>>
                    <div class="title-container naglowek-napis">
                        <div class="title-content ">
                            <?php echo $header->getVitamedText(); ?>
                            <?php echo $header->getTitleText(); ?>
                        </div>
                        <a class="header-button-link" href="#">

                            <?php echo $header->getButton(); ?>



                        </a>
                    </div>
                    <?php if ($header->isProduct()) {  ?>
                        <div class="image-container">
                            <?php if (get_field('nowosc')) { ?>
                                <img src="<?php bloginfo('template_directory'); ?>/assets/img/nowosc@2.png" class="nowosc" />
                            <?php } ?>

                            <img class="product-image" src="<?php echo the_post_thumbnail_url('full'); ?>" />
                        </div>
                    <?php } ?>
                    <?php if ($header->requireCarousel()) {  ?>
                        <!---->
                        <!--                        <div class="title-container karuzela-efekt">-->
                        <!--                                    <div class="karuzela ">-->
                        <!--                                        <div class="carousel pyk" data-flickity='{"imagesLoaded":true, "arrowShape": "M 12.881 47.156 L 41.738 20.164 L 48.024 26.125 L 32.959 41.407 L 84.983 41.407 L 84.983 52.571 L 32.923 52.636 L 47.184 65.096 L 42.005 72.977 Z" , "groupCells": true, "autoPlay": true, "prevNextButtons": true, "pageDots": false, "freeScroll":false, "contain":true, "wrapAround":true } '>-->
                        <!--                                            --><?php //while ($products->have_posts()) : $products->the_post();
                                                                            ?>
                        <!---->
                        <!--                                                <div class="carousel-cell col">-->
                        <!--                                                    <a href="--><?php //the_permalink();
                                                                                            ?>
                        <!--" style="text-decoration: none;">-->
                        <!--                                                    <div class="img-carosell ">-->
                        <!--                                                        <img class="" src="--><?php //the_post_thumbnail_url('medium');
                                                                                                            ?>
                        <!--" />-->
                        <!--                                                    </div>-->
                        <!--                                                    <h5 class="card-title font-weight-bold">--><?php //the_title();
                                                                                                                            ?>
                        <!--</h5>-->
                        <!--                                                    <div class="text-center">--><?php //the_excerpt($post);
                                                                                                            ?>
                        <!--</div>-->
                        <!--                                                    <a href="--><?php //the_permalink();
                                                                                            ?>
                        <!--" class="product-button">Sprawdź</a>-->
                        <!--                                                    </a>-->
                        <!---->
                        <!--                                                </div>-->
                        <!--                                            --><?php //endwhile;
                                                                            ?>
                        <!--                                        </div>-->
                        <!--                                    </div>-->
                        <!--                        </div>-->
                        <div class="title-container karuzela-efekt">
                            <div class="karuzela ">
                                <div class="carousel pyk" data-flickity='{"imagesLoaded":true, "arrowShape": "M 12.881 47.156 L 41.738 20.164 L 48.024 26.125 L 32.959 41.407 L 84.983 41.407 L 84.983 52.571 L 32.923 52.636 L 47.184 65.096 L 42.005 72.977 Z" , "groupCells": true, "autoPlay": true, "prevNextButtons": true, "pageDots": false, "freeScroll":false, "contain":true, "wrapAround":true } '>
                                    <?php while ($products->have_posts()) : $products->the_post(); ?>

                                        <div class="carousel-cell h-100 col ">
                                            <div class="card h-100 align-items-center" style="border: none; background: transparent;">
                                                <a href="<?php the_permalink(); ?>" style="text-decoration: none;">
                                                    <img class="card-img-top " style="height: 250px; object-fit: contain;" src="<?php the_post_thumbnail_url('full'); ?>" alt="Card image cap">
                                                    <div class="card-body flex-column d-flex">
                                                        <h5 class="card-title font-weight-bold"><?php the_title(); ?></h5>
                                                        <div class="text-center"><?php the_excerpt($post); ?></div>
                                                        <?php
                                                        if (DefaultHelper::checkEn() == 'en') { ?>
                                                            <a href="<?php the_permalink(); ?>" class="product-button">Check it out</a>
                                                        <?php } else { ?>
                                                            <a href="<?php the_permalink(); ?>" class="product-button">Sprawdź</a>
                                                        <?php } ?>
                                                </a>
                                            </div>
                                        </div>
                                </div>
                            <?php endwhile; ?>
                            </div>
                        </div>
                </div>
            <?php } ?>

            </div>
        </div>
        </div>
    </header>
    <?php if ($header->isProduct()) { ?>
        <style>
            @media (min-resolution: 1.5dppx) and (min-width: 1000px) {
                .header-mask {
                    height: auto !important;
                }

                .header {
                    height: auto !important;
                }
            }
        </style>
    <?php } else { ?>
        <style>
            @media (min-resolution: 1.5dppx) and (min-width: 1000px) {
                .header-mask {
                    height: 600px;
                }

                .header {
                    height: 600px;
                }
            }
        </style>
    <?php } ?>

    <div class="ozdobnik-separator" style="transform: translateY(-99%); position: absolute;"><img style="max-width: 100%;" src="<?php bloginfo('template_directory'); ?>/assets/img/separator1.png" /></div>