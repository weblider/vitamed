<?php
/*
 * Template Name: Współpraca
 */

get_header();
$news = ProductsService::getNews(1, 3);
?>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/kariera.css" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/single.css" />


<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
    <div class="body">
        <div class="container">
            <div class="clear"></div>
            <div class="main wspolpraca-main">
                <div class="row mt-5" style="margin-bottom: -130px;">
                    <div class="col d-flex justify-cetner align-items-center flex-column">
                        <?php the_content(); ?>
                        <img class="pic pt-3 w-25" src="<?php bloginfo('template_directory'); ?>/assets/img/wspolpraca.png" />

                    </div>
                    <div class="col position-relative">
                        <div class="kariera ml-2">
                            <div class="text-center">
                                <!--                            <img class="ozdobnik-main" src="--><?php //bloginfo('template_directory') 
                                                                                                    ?>
                                <!--/assets/img/ozdobnik4.png">-->
                                <img class="w-25" src="<?php bloginfo('template_directory'); ?>/assets/img/symbol-formularz.png" />
                            </div>
                            <div class="text-center mb-3">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    <p>Send message</p>
                                    <h4>Contact with us</h4>
                                <?php } else { ?>
                                    <p>Wyślij wiadomość</p>
                                    <h4>Skontaktuj się z nami</h4>
                                <?php } ?>

                            </div>
                            <!-- <form>
                            <div class="form-group">
                                <label for="username">Nazwa firmy: *</label>
                                <input type="username" class="form-control" id="username"  required>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Partner: </label>
                                <select class="form-control" id="exampleFormControlSelect1">
                                    <option>Hurtownia</option>
                                    <option>Producent</option>
                                    <option>Apteka</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="username">Imię i nazwisko: *</label>
                                <input type="username" class="form-control" id="username" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Adres e-mail: * </label>
                                <input type="email" class="form-control" id="email"  required>
                            </div>
                            <div class="form-group">
                                <label for="phone">Telefon: *</label>
                                <input type="text" class="form-control" id="phone" required>
                            </div>
                            <div class="form-group">
                                <label for="message">Wiadomość: *</label>
                                <textarea type="" class="form-control" id="message" required></textarea>
                            </div>
                            <button type="submit" class="btn btn-success w-100 mt-5">WYŚLIJ</button>
                            <small>* - pola obowiązkowe</small>
                            </div>
                        <div class="form-check">-->
                            <!--                                <input type="checkbox" class="form-check-input" id="exampleCheck1">-->
                            <!--                                <label class="form-check-label" for="exampleCheck1">-->
                            <!--                                    Wyrażam zgodę na przetwarzanie danych osobowych zawartychw mojej ofercie prac przez VITAMED PHARMACEUTICALS Sp. z o. o. </label>-->
                            <!--                            </div>-->

                            <!-- </form> -->

                            <form action="/wspolpraca/#wpcf7-f2092-p84-o1" method="post" id="validate" class="wpcf7-form init" novalidate="novalidate" data-status="init">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="2092">
                                    <input type="hidden" name="_wpcf7_version" value="5.4">
                                    <input type="hidden" name="_wpcf7_locale" value="pl_PL">
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f2092-p84-o1">
                                    <input type="hidden" name="_wpcf7_container_post" value="84">
                                    <input type="hidden" name="_wpcf7_posted_data_hash" value="">
                                </div>
                                <div class="form-group">
                                    <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                        <p><label class="w-100"> Company: *<br>
                                            <?php } else { ?>
                                                <p><label class="w-100"> Nazwa firmy: *<br>
                                                    <?php } ?>
                                                    <span class="wpcf7-form-control-wrap company"><input type="text" name="company" value="" size="40" class="form-control wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="company" aria-required="true" aria-invalid="false"></span> </label></p>
                                </div>
                                <div class="form-group">
                                    <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                        <p><label class="w-100"> Partner:<br>
                                                <span class="wpcf7-form-control-wrap partner"><select name="partner" id="partner" class="w-100 wpcf7-form-control wpcf7-select" aria-invalid="false">
                                                        <option value="Hurtownia">Wholesale</option>
                                                        <option value="Producent">Manufacturer</option>
                                                        <option value="Apteka">Pharmacy</option>
                                                    </select></span> </label></p>
                                    <?php } else { ?>
                                        <p><label class="w-100"> Partner:<br>
                                                <span class="wpcf7-form-control-wrap partner"><select name="partner" id="partner" class="w-100 wpcf7-form-control wpcf7-select" aria-invalid="false">
                                                        <option value="Hurtownia">Hurtownia</option>
                                                        <option value="Producent">Producent</option>
                                                        <option value="Apteka">Apteka</option>
                                                    </select></span> </label></p>
                                </div>
                            <?php } ?>

                            <div class="form-group">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    <p><label class="w-100"> Name: *<br>
                                            <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" id="your-name" value="" size="40" class="form-control wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span> </label></p>
                                <?php } else { ?>
                                    <p><label class="w-100"> Imię i nazwisko: *<br>
                                            <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" id="your-name" value="" size="40" class="form-control wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="state" aria-required="true" aria-invalid="false"></span> </label></p>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    <p><label class="w-100"> E-mail: *<br>
                                            <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" id="your-email" value="" size="40" class="form-control wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span> </label></p>
                                <?php } else { ?>
                                    <p><label class="w-100"> Adres e-mail: *<br>
                                            <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" id="your-email" value="" size="40" id="email" class="form-control wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span> </label></p>
                                <?php } ?>

                            </div>
                            <div class="form-group">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    <p><label class="w-100"> Phone: *<br>
                                            <span class="wpcf7-form-control-wrap tel"><input type="tel" name="tel" id="tel" value="" size="40" class="form-control wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false"></span> </label></p>
                                <?php } else { ?>
                                    <p><label class="w-100"> Telefon: *<br>
                                            <span class="wpcf7-form-control-wrap tel"><input type="tel" name="tel" id="tel" value="" id="phone" size="40" class="form-control wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false"></span> </label></p>
                                <?php } ?>

                            </div>
                            <div class="form-group">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    <p><label class="w-100"> Message: *<br>
                                            <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" id="your-message" cols="10" rows="5" class="form-control wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></textarea></span> </label></p>
                                <?php } else { ?>
                                    <p><label class="w-100"> Wiadomość: *<br>
                                            <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" id="your-message" cols="10" rows="5" class="form-control wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></textarea></span> </label></p>
                                <?php } ?>

                            </div>
                            <div class="form-group">
                                <p><label class="w-100">
                                        <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                            <label><span class="wpcf7-form-control-wrap acceptance"><span class="wpcf7-form-control wpcf7-acceptance"><span class="wpcf7-list-item"><label><input type="checkbox" name="acceptance" id="acceptance" value="1" aria-invalid="false"><span class="wpcf7-list-item-label">I agree to have my personal data processed by VITAMED PHARMACEUTICALS Sp. z o.o. *</span></label></span></span></span><br>
                                            <?php } else { ?>
                                                <label><span class="wpcf7-form-control-wrap acceptance"><span class="wpcf7-form-control wpcf7-acceptance"><span class="wpcf7-list-item"><label><input type="checkbox" name="acceptance" id="acceptance" value="1" id="my-checkbox" aria-invalid="false"><span class="wpcf7-list-item-label">Wyrażam zgodę na przetwarzanie danych osobowych przez VITAMED PHARMACEUTICALS Sp. z o. o. *</span></label></span></span></span><br>
                                                </label>
                                            <?php } ?>


                                            <span class="wpcf7-form-control-wrap checkbox"><span class="wpcf7-form-control wpcf7-checkbox wpcf7-validates-as-required"></span></span><br>
                                            </label></p>
                            </div>
                            <div class="form-group">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    <p class="btn-text"><input type="submit" value="SUBMIT" class="wpcf7-form-control wpcf7-submit w-50"></p>
                                <?php } else { ?>
                                    <p class="btn-text"><input type="submit" value="WYŚLIJ" class="wpcf7-form-control wpcf7-submit w-50"></p>
                                <?php } ?>

                                <div class="wpcf7-response-output" aria-hidden="true"></div>
                            </div>
                            <div class="alert alert-success" style="display: none;" role="alert">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    Form was sent
                                <?php } else { ?>
                                    Formularz wysłany
                                <?php } ?>
                            </div>
                            <div class="alert alert-danger" style="display: none;" role="alert">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    Fill all empty inputs!
                                <?php } else { ?>
                                    Uzupełnij wszystkie dane!
                                <?php } ?>
                            </div>
                            <p>* - pola wymagane</p>
                            </form>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php endwhile; ?>
<script>
    function sleep(milliseconds) {
        const date = Date.now();
        let currentDate = null;
        do {
            currentDate = Date.now();
        } while (currentDate - date < milliseconds);
    }
</script>
<script>
    $("#validate").submit(function() {
        if (document.getElementById("company").value.length == 0 || document.getElementById("partner").value.length == 0 || document.getElementById("your-name").value.length == 0 || document.getElementById("your-email").value.length == 0 || document.getElementById("tel").value.length == 0 || document.getElementById("your-message").value.length == 0 || document.getElementById('acceptance').checked == false) {
            $('.alert-danger').css("display", "block");
            return false;
        } else {
            $('.alert-danger').css("display", "none");
            $('.alert-success').css("display", "block");
            sleep(2000);
            return true;
        }
    });
</script>
<?php get_footer();
