<?php
/*
 * Template Name: Lista produktów
 * Template Post Type: page
 */
$productsHome = ProductsService::getPagesByTemplate('products.php');
get_header();
?>
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/products-list.css"/>


    <div class="container ">
        <div class="row symbols-row">
			<?php foreach ($productsHome as $pr) { ?>
                <div onclick="href('<?php echo get_permalink($pr->ID); ?>')" class="col-lg-4 col-md-4 col-sm-6 border-spec hover-<?php echo $pr->ID; ?>">
                    <img class="symbol w-50" src="<?php echo get_the_post_thumbnail_url($pr->ID); ?>">
                    <p class="title"><?php echo $pr->post_title; ?></p>
                    <a class="btn btn-primary arrow" href="<?php echo get_the_permalink($pr->ID); ?>">
                        <img class="w-75" src="<?php bloginfo('template_directory'); ?>/assets/img/strzalka4@2.png">
                    </a>
                    <?php if (DefaultHelper::checkEn() == 'en') { ?>
                        <p class="quantity pt-1"><img src="<?php bloginfo('template_directory'); ?>/assets/img/ikona2.png" /> <?php echo DefaultHelper::getCountFromCategoryEnglish($pr->post_title); ?></p>
                    <?php } else { ?>
                        <p class="quantity pt-1"><img src="<?php bloginfo('template_directory'); ?>/assets/img/ikona2.png" /> <?php echo DefaultHelper::getCountFromCategory($pr->post_title); ?></p>
                    <?php } ?>
                </div>
                <script>
                    function href(element){
                        window.location.href = element;
                    }
                    $('.hover-<?php echo $pr->ID; ?>').on('mouseover', function() {
                        $(this).find('.symbol').attr('src', '<?php bloginfo('template_directory'); ?>/assets/img/Ikony/<?php echo $pr->post_title; ?>-green.svg');
                    });

                    $('.hover-<?php echo $pr->ID; ?>').on('mouseleave', function() {
                        $(this).find('.symbol').attr('src', '<?php echo get_the_post_thumbnail_url($pr->ID); ?>');
                    });
                </script>
			<?php } ?>
        </div>
    </div>


<?php get_footer();
