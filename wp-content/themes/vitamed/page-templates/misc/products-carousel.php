<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
<script src="<?php bloginfo('template_directory'); ?>/assets/js/flickity.js"></script>


<div class="ramka-1">
    <div class="ramka-2">
        <div class="karuzela">
            <div class="carousel pyk w-75" data-flickity='{ "groupCells": true, "prevNextButtons": true, "pageDots": false, "freeScroll":false, "contain":true, "wrapAround":true }'>
                    <div class="carousel-cell" >
                        <div class="img-carosell ">
                            <img class="" src="<?php the_post_thumbnail_url( 'medium' ); ?>" />
                        </div>
                        <h5 class="card-title"><?php the_title(); ?></h5>
                    </div>
            </div>
        </div>
    </div>
</div>

