<script>
    $(document).ready(function() {
//Preloader
        preloaderFadeOutTime = 1100;
        function hidePreloader() {
            var preloader = $('.spinner-wrapper');
            preloader.fadeOut(preloaderFadeOutTime);
        }
        hidePreloader();
    });
</script>

<style>
	.spinner-wrapper {
		position: fixed;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		background-color: #a0bd49;
		z-index: 999999;
	}

	.spinner{
		display: flex;
		justify-content: center;
		align-items: center;
		width: 100%;
		height: 100%;
	}
	.spinner img{
		width:100px;
		box-shadow: 0 0 0 0 rgba(0, 0, 0, 1);
		transform: scale(0.9);
		animation: pulse 5s infinite;
	}

	@keyframes pulse {
		0% {
			transform: scale(0.65);
			/*box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.7);*/
		}

		70% {
			transform: scale(1.5);
			/*box-shadow: 0 0 0 10px rgba(0, 0, 0, 0);*/
		}

		100% {
			transform: scale(0.65);
			/*box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);*/
		}
	}


</style>