

<style>
    .partnerzy .carousel-cell {
        padding: 20px 35px 35px 35px;
    }
</style>
<div class="partnerzy container-fluid text-center pt-1 pb-3" style="color: #e7e7e7">
	<?php if (DefaultHelper::checkEn() == 'en'){ ?>
        <p class="grey-text mt-2">Brands from our portfolio:</p>
	<?php }else{ ?>
        <p class="grey-text mt-2">Marki z naszego portfolio:</p>
	<?php } ?>

    <div class="carousel" data-flickity='{"autoPlay":2500, "pauseAutoPlayOnHover": false,"imagesLoaded":true, "groupCells": 2, "prevNextButtons": false, "pageDots": false, "freeScroll":true, "contain":true, "wrapAround":true }'>
        <div class="carousel-cell"><img class="" src="<?php bloginfo('template_directory'); ?>/assets/img/Loga/colinox.png" /></div>
        <div class="carousel-cell"><img class="" src="<?php bloginfo('template_directory'); ?>/assets/img/Loga/cue.png" /></div>
        <div class="carousel-cell"><img class="" src="<?php bloginfo('template_directory'); ?>/assets/img/Loga/dermicin.png" /></div>
        <div class="carousel-cell"><img class="" src="<?php bloginfo('template_directory'); ?>/assets/img/Loga/emofix.png" /></div>
        <div class="carousel-cell"><img class="" src="<?php bloginfo('template_directory'); ?>/assets/img/Loga/gastrotuss.png" /></div>
        <div class="carousel-cell"><img class="" src="<?php bloginfo('template_directory'); ?>/assets/img/Loga/gripstop.png" /></div>
        <div class="carousel-cell"><img class="" src="<?php bloginfo('template_directory'); ?>/assets/img/Loga/nailner.png" /></div>
        <div class="carousel-cell"><img class="" src="<?php bloginfo('template_directory'); ?>/assets/img/Loga/noctituss.png" /></div>
        <div class="carousel-cell"><img class="" src="<?php bloginfo('template_directory'); ?>/assets/img/Loga/mysticker-pro.png" /></div>
        <div class="carousel-cell"><img class="" src="<?php bloginfo('template_directory'); ?>/assets/img/Loga/poxclin.png" /></div>
        <div class="carousel-cell"><img class="" src="<?php bloginfo('template_directory'); ?>/assets/img/Loga/puranox.png" /></div>
        <div class="carousel-cell"><img class="" src="<?php bloginfo('template_directory'); ?>/assets/img/Loga/rinocross.png" /></div>
        <div class="carousel-cell"><img class="" src="<?php bloginfo('template_directory'); ?>/assets/img/Loga/rinopanteina.png" /></div>
        <div class="carousel-cell"><img class="" src="<?php bloginfo('template_directory'); ?>/assets/img/Loga/rowatinex.png" /></div>
        <div class="carousel-cell"><img class="" src="<?php bloginfo('template_directory'); ?>/assets/img/Loga/spotner.png" /></div>
        <div class="carousel-cell"><img class="" src="<?php bloginfo('template_directory'); ?>/assets/img/Loga/stomatovis.png" /></div>
        <div class="carousel-cell"><img class="" style="margin-top: -19%;" src="<?php bloginfo('template_directory'); ?>/assets/img/Loga/wortie.png" /></div>
    </div>
</div>