<?php
/*
 * Template Name: Kariera
 */

get_header();

?>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/kariera.css" />

<div class="body">
    <div class="container">
        <div class="clear"></div>
        <div class="main kariera-main">
            <div class="row mt-5">
                <div class="col left">
                    <div class="wp-content">
                        <?php the_content(); ?>
                    </div>
                    <?php if (DefaultHelper::checkEn() == 'en') { ?>
                        <div class="row text-center benefits">
                            <div class="col">
                                <img class="pic" src="<?php bloginfo('template_directory'); ?>/assets/img/zaleta-umowa-o-prace@2.png" />
                                <p>Contract of employment</p>
                            </div>
                            <div class="col">
                                <img class="pic" src="<?php bloginfo('template_directory'); ?>/assets/img/zaleta-atrakcyjne-wynagrodzenie@2.png" />
                                <p>Attractive bonus system</p>
                            </div>
                        </div>
                        <div class="row text-center benefits">
                            <div class="col">
                                <img class="pic" src="<?php bloginfo('template_directory'); ?>/assets/img/zaleta-samochod-firmowy@2.png" />
                                <p>Company car</p>
                            </div>
                            <div class="col">
                                <img class="pic" src="<?php bloginfo('template_directory'); ?>/assets/img/zaleta-pakiet-opieki-medycznej@2.png" />
                                <p>Medical care package</p>
                            </div>
                        </div>
                        <div class="row text-center benefits">
                            <div class="col" style="margin-top: 1.5rem;">
                                <img class="pic" src="<?php bloginfo('template_directory'); ?>/assets/img/zaleta-karta-multisport@2.png" />
                                <p>Multisport Card</p>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="row text-center benefits">
                            <div class="col">
                                <img class="pic" src="<?php bloginfo('template_directory'); ?>/assets/img/zaleta-umowa-o-prace@2.png" />
                                <p>Umowa o pracę</p>
                            </div>
                            <div class="col">
                                <img class="pic" src="<?php bloginfo('template_directory'); ?>/assets/img/zaleta-atrakcyjne-wynagrodzenie@2.png" />
                                <p>Atrakcyjny system premiowy</p>
                            </div>
                        </div>
                        <div class="row text-center benefits">
                            <div class="col">
                                <img class="pic" src="<?php bloginfo('template_directory'); ?>/assets/img/zaleta-samochod-firmowy@2.png" />
                                <p>Samochód służbowy</p>
                            </div>
                            <div class="col">
                                <img class="pic" src="<?php bloginfo('template_directory'); ?>/assets/img/zaleta-pakiet-opieki-medycznej@2.png" />
                                <p>Pakiet opieki medycznej</p>
                            </div>
                        </div>
                        <div class="row text-center benefits">
                            <div class="col" style="margin-top: 1.5rem;">
                                <img class="pic" src="<?php bloginfo('template_directory'); ?>/assets/img/zaleta-karta-multisport@2.png" />
                                <p>Karta multisport</p>
                            </div>
                        </div>
                    <?php } ?>

                </div>
                <div class="col position-relative">
                    <div class="kariera ml-2">
                        <div class="text-center">
                            <!--                            <img class="ozdobnik-main" src="--><?php //bloginfo('template_directory') 
                                                                                                ?>
                            <!--/assets/img/ozdobnik4.png">-->
                            <img class="w-25" src="<?php bloginfo('template_directory'); ?>/assets/img/symbol-formularz.png" />
                        </div>
                        <div class="text-center mb-3">
                            <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                <p class="title">Open recruitment</p>
                                <h4>Send your application</h4>
                            <?php } else { ?>
                                <p class="title">Otwarta Rekrutacja</p>
                                <h4>Prześlij swoją aplikację</h4>
                            <?php } ?>

                        </div>

                        <form action="/kariera/#wpcf7-f1869-o2" method="post" id="validate" class="wpcf7-form init" enctype="multipart/form-data" novalidate="novalidate" data-status="init">
                            <div style="display: none;">
                                <input type="hidden" name="_wpcf7" value="1869">
                                <input type="hidden" name="_wpcf7_version" value="5.4">
                                <input type="hidden" name="_wpcf7_locale" value="pl_PL">
                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1869-o2">
                                <input type="hidden" name="_wpcf7_container_post" value="0">
                                <input type="hidden" name="_wpcf7_posted_data_hash" value="">
                            </div>
                            <div class="form-group">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    <p><label class="w-100"> Your Name: *<br>
                                        <?php } else { ?>
                                            <p><label class="w-100"> Imię i nazwisko: *<br>
                                                <?php } ?>

                                                <span class="wpcf7-form-control-wrap surname"><input class="form-control" type="text" name="surname" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required " id="surname" aria-required="true" aria-invalid="false" required></span> </label></p>
                            </div>
                            <div class="form-group">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    <p><label class="w-100"> State: *<br>
                                        <?php } else { ?>
                                            <p><label class="w-100"> Województwo: *<br>
                                                <?php } ?>
                                                <span class="wpcf7-form-control-wrap voivodeship"><input class="form-control" type="text" name="voivodeship" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required " id="state" aria-required="true" aria-invalid="false" required></span> </label></p>
                            </div>
                            <div class="form-group">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    <p><label class="w-100"> Phone: *<br>
                                        <?php } else { ?>
                                            <p><label class="w-100"> Telefon: *<br>
                                                <?php } ?>

                                                <span class="wpcf7-form-control-wrap phone-number"><input class="form-control" type="tel" name="phone-number" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel  klasa" id="phone" aria-required="true" aria-invalid="false" required></span> </label></p>
                            </div>
                            <div class="form-group">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    <p><label class="w-100"> E-mail: *<br>
                                        <?php } else { ?>
                                            <p><label class="w-100"> Adres e-mail: *<br>
                                                <?php } ?>

                                                <span class="wpcf7-form-control-wrap email"><input class="form-control" type="email" name="email" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email " id="email" aria-required="true" aria-invalid="false" required></span> </label></p>
                            </div>
                            <div class="form-group">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    <p><label class="w-100 file-input">Send CV: *<br>
                                        <?php } else { ?>
                                            <p><label class="w-100 file-input "> Prześlij CV: *<br>
                                                <?php } ?>
                                                <svg class="mt-2" style="fill: #97bf20;" height="46" viewBox="0 0 512 512" width="46" xmlns="http://www.w3.org/2000/svg">
                                                    <g id="Solid">
                                                        <path d="m239.029 384.97a24 24 0 0 0 33.942 0l90.509-90.509a24 24 0 0 0 0-33.941 24 24 0 0 0 -33.941 0l-49.539 49.539v-262.059a24 24 0 0 0 -48 0v262.059l-49.539-49.539a24 24 0 0 0 -33.941 0 24 24 0 0 0 0 33.941z" />
                                                        <path d="m464 232a24 24 0 0 0 -24 24v184h-368v-184a24 24 0 0 0 -48 0v192a40 40 0 0 0 40 40h384a40 40 0 0 0 40-40v-192a24 24 0 0 0 -24-24z" />
                                                    </g>
                                                </svg>

                                                <span class="wpcf7-form-control-wrap cv">
                                                    <input type="file" name="cv" size="40" id="file-upload" class="wpcf7-form-control wpcf7-file wpcf7-validates-as-required" accept=".jpg,.jpeg,.png,.gif,.pdf,.doc,.docx,.ppt,.pptx,.odt,.avi,.ogg,.m4a,.mov,.mp3,.mp4,.mpg,.wav,.wmv,.doc,.docx,.odt" aria-required="true" aria-invalid="false" required>
                                                    <p>Załączony plik: <span class="cv-cv"></span></p>
                                                </span>

                                                </label></p>
                            </div>
                            <div class="form-group ">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    <p><label class="w-100 file-input"> Submit a cover letter:<br>
                                        <?php } else { ?>
                                            <p><label class="w-100 file-input "> Prześlij list motywacyjny:<br>
                                                <?php } ?>

                                                <svg class="mt-2" style="fill: #97bf20;" height="46" viewBox="0 0 512 512" width="46" xmlns="http://www.w3.org/2000/svg">
                                                    <g id="Solid">
                                                        <path d="m239.029 384.97a24 24 0 0 0 33.942 0l90.509-90.509a24 24 0 0 0 0-33.941 24 24 0 0 0 -33.941 0l-49.539 49.539v-262.059a24 24 0 0 0 -48 0v262.059l-49.539-49.539a24 24 0 0 0 -33.941 0 24 24 0 0 0 0 33.941z" />
                                                        <path d="m464 232a24 24 0 0 0 -24 24v184h-368v-184a24 24 0 0 0 -48 0v192a40 40 0 0 0 40 40h384a40 40 0 0 0 40-40v-192a24 24 0 0 0 -24-24z" />
                                                    </g>
                                                </svg>

                                                <span class="wpcf7-form-control-wrap cover">
                                                    <input type="file" name="cover" id="file-upload2" size="40" class="wpcf7-form-control wpcf7-file" accept=".jpg,.jpeg,.png,.gif,.pdf,.doc,.docx,.ppt,.pptx,.odt,.avi,.ogg,.m4a,.mov,.mp3,.mp4,.mpg,.wav,.wmv,.doc,.docx,.odt" aria-invalid="false">
                                                    <p>Załączony plik: <span class="mot-mot"></span></p>
                                                </span>
                                                </label>

                                            </p>
                            </div>
                            <div class="form-group">
                                <p><label class="w-100">
                                        <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                            <label><span class="wpcf7-form-control-wrap acceptance"><span class="wpcf7-form-control wpcf7-acceptance"><span class="wpcf7-list-item"><label><input type="checkbox" name="acceptance" id="acceptance" value="1" aria-invalid="false"><span class="wpcf7-list-item-label">I agree to have my personal data included in my job offer processed by VITAMED PHARMACEUTICALS Sp. z o.o. for the needs of the recruitment process (in accordance with the Personal Data Protection Act of 29.08.97, Journal of Laws No. 133 item 883) *</span></label></span></span></span><br>
                                            <?php } else { ?>
                                                <label><span class="wpcf7-form-control-wrap acceptance"><span class="wpcf7-form-control wpcf7-acceptance"><span class="wpcf7-list-item"><label><input id="my-checkbox" type="checkbox" name="acceptance" id="acceptance" value="1" aria-invalid="false"><span class="wpcf7-list-item-label">Wyrażam zgodę na przetwarzanie danych osobowych zawartych w mojej ofercie prac przez VITAMED PHARMACEUTICALS Sp. z o. o. dla potrzeb niezbędnych do realizacji procesu rekrutacji (zgodnie z Ustawą z dnia 29.08.97 o ochronie danych osobowych, Dziennik Ustaw Nr. 133 Poz. 883) *</span></label></span></span></span><br>
                                                </label>
                                            <?php } ?>


                                            <span class="wpcf7-form-control-wrap checkbox"><span class="wpcf7-form-control wpcf7-checkbox wpcf7-validates-as-required"></span></span><br>
                                            </label></p>
                            </div>
                            <div class="form-group">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    <p><input type="submit" value="SUBMIT" class="wpcf7-form-control wpcf7-submit w-50"></p>
                                <?php } else { ?>
                                    <p><input type="submit" value="WYŚLIJ" class="wpcf7-form-control wpcf7-submit w-50"></p>
                                <?php } ?>

                                <div class="wpcf7-response-output" aria-hidden="true">
                                </div>
                            </div>
                            <div class="alert alert-success" style="display: none;" role="alert">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    Form was sent
                                <?php } else { ?>
                                    Formularz wysłany
                                <?php } ?>

                            </div>
                            <div class="alert alert-danger" style="display: none;" role="alert">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    Fill all empty inputs!
                                <?php } else { ?>
                                    Uzupełnij wszystkie dane!
                                <?php } ?>
                            </div>
                            <p>* - pola wymagane</p>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#file-upload').change(function() {
        var i = $(this).prev('label').clone();
        var file = $('#file-upload')[0].files[0].name;
        $('.cv-cv').text(file);
    });
    $('#file-upload2').change(function() {
        var i = $(this).prev('label').clone();
        var file = $('#file-upload2')[0].files[0].name;
        $('.mot-mot').text(file);
    });

    function sleep(milliseconds) {
        const date = Date.now();
        let currentDate = null;
        do {
            currentDate = Date.now();
        } while (currentDate - date < milliseconds);
    }
</script>
<script>
    $("#validate").submit(function() {
        console.log("hejo")
        if (document.getElementById("surname").value.length == 0 || document.getElementById("state").value.length == 0 || document.getElementById("phone").value.length == 0 || document.getElementById("email").value.length == 0 || document.getElementById("file-upload").value.length == 0 || document.getElementById('acceptance').checked == false) {
            console.log("hejjj")
            $('.alert-danger').css("display", "block");
            return false;
        } else {
            $('.alert-danger').css("display", "none");
            $('.alert-success').css("display", "block");
            sleep(2000);
            return true;
        }
    });
</script>
<?php get_footer();
