<?php
/*
 * Template Name: Aktualnosci
 */

get_header();

$posts = ProductsService::getNews(null,-1);

?>

<?php if(DefaultHelper::checkEn() == 'en'){ ?>
<style>
    .header-image{
        background-image: url("/wp-content/themes/vitamed/assets/img/tlo-aktualnosci.jpg") !important;
    }
</style>
<?php } ?>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/news.css"/>


<main role="main" class="w-100">
<div class="container mb-5 container-aktualnosci">
    <div class="row news-list mb-5">
        <?php while ( $posts->have_posts() ) : $posts->the_post(); ?>
            <div class="col-lg-4 col-md-6 col-sm-12 mb-5" style="">
                <a style="text-decoration: none; color: black" href="<?php the_permalink();?>">
            <div class="card  d-table-cell align-middle h-100">
                <div class="position-relative text-center">
                    <img class="card-img-top" src="<?php the_post_thumbnail_url( 'medium' ); ?>" alt="Card image cap">
                    <!-- <div class="data"><?php echo get_the_date( 'd.m.Y' ); ?></div> -->
                </div>
                <div class="card-body d-flex flex-column">
                    <h5 class="card-title"><?php the_title(); ?></h5>
                    <p class="card-text mt-auto"><?php the_excerpt($post); ?></p>
                </div>
            </div>
                </a>
        </div>
        <?php endwhile; ?>
    </div>

</div>
</main>

<?php get_footer();
