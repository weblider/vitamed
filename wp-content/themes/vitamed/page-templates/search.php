<?php
/*
 * Template Name: Wyszukiwarka
 */

get_header();
require_once('wp-load.php'); // add wordpress functionality
$post = $_POST['slug'];

$args = array(
	'post_type'=> 'post',
	'orderby'    => 'date',
	'post_status' => 'publish',
	'order'    => 'DESC',
	's' => $_POST['slug'],
//	'offset' => $offset,
	'posts_per_page' => -1,
);


//die;
$result = new WP_Query( $args );

//print_r($_GET['slug']);

?>
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/search.css"/>

    <div class="body">
        <div class="container">
            <div class="clear"></div>
            <div class="main">
                <div class="row mt-5 w-100">
					<?php if ( $result->have_posts() ) : ?>
						<?php while ( $result->have_posts() ) : $result->the_post(); ?>
                            <div class="d-flex justify-content-center align-items-center w-100 pt-3 mt-1 mb-1 pb-3">
                                <div style="width: 250px; height: 250px; background-size: contain; background-repeat: no-repeat; background-image: url(<?php the_post_thumbnail_url( 'medium' ); ?>);">
                                </div>
                                <div class="text float-left w-50 pr-5 pl-5 mt-2 pt-2">
                                    <a href="<?php the_permalink();?>">
                                        <h5 class="card-title"><?php the_title(); ?></h5>
                                        <p class="card-text mt-auto"><?php the_excerpt($post); ?></p>
                                    </a>
                                </div>
                            </div>
                            <div class="clear"></div>

						<?php endwhile; ?>
					<?php else : ?>
                        <div class="row w-100">
                            <h3 class="pt-5 pb-5">Nic tu nie ma ...</h3>
                        </div>
					<?php endif; ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

<style>
    @media(max-width: 600px){
        .d-flex{
            flex-direction: column;
        }
    }
</style>
<?php get_footer();
