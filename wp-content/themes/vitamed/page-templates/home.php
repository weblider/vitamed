<?php
/*
 * Template Name: Strona główna
 */

get_header();

$productsHome = ProductsService::getPagesByTemplate('products.php');
$news = ProductsService::getNews(1, 3);
$products = ProductsService::getProducts(null, -1, 'Popularne');
$firstProduct = ProductsService::getNews(0, 1);
$postsStrefa = ProductsService::getProducts(null, -1, 'Strefa Pacjenta')->posts;

?>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/home.css" />



<main role="main" class="w-100">
    <div class="container ">
        <div class="row next-section for-carousel">
            <div class="col mb-5">
                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                    <p class="text">We are a dynamically developing company on the market of pharmaceuticals with a Polish capital.</b></p>
                    <p class="subtext">For many years we have been providing our customers with the highest quality medicinal products.</p>
                <?php } else { ?>
                    <p class="text">Jesteśmy dynamicznie rozwijającą się firmą branży farmaceutycznej <b>z wyłącznie polskim kapitałem.</b></p>
                    <p class="subtext">Od wielu lat zapewniamy naszym klientom najwyższej jakości produkty lecznicze.</p>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="container ">
        <div class="row symbols-row">
            <?php foreach ($productsHome as $pr) { ?>
                <div onclick="href('<?php echo get_the_permalink($pr->ID); ?>')" class="col-lg-4 col-md-6 col-sm-6 border-spec hover-<?php echo $pr->ID; ?>">
                    <img class="symbol w-50" src="<?php echo get_the_post_thumbnail_url($pr->ID); ?>">
                    <p class="title"><?php echo $pr->post_title; ?></p>
                    <a class="btn btn-primary arrow" href="<?php echo get_the_permalink($pr->ID); ?>">
                        <img class="w-100" src="<?php bloginfo('template_directory'); ?>/assets/img/strzalka4@2.png">
                    </a>
                    <?php if (DefaultHelper::checkEn() == 'en') { ?>
                        <p class="quantity pt-1"><img src="<?php bloginfo('template_directory'); ?>/assets/img/ikona2.png" /> <?php echo DefaultHelper::getCountFromCategoryEnglish($pr->post_title); ?></p>
                    <?php } else { ?>
                        <p class="quantity pt-1"><img src="<?php bloginfo('template_directory'); ?>/assets/img/ikona2.png" /> <?php echo DefaultHelper::getCountFromCategory($pr->post_title); ?></p>
                    <?php } ?>
                </div>
                <script>
                    function href(element) {
                        window.location.href = element;
                    }
                    $('.hover-<?php echo $pr->ID; ?>').on('mouseover', function() {
                        $(this).find('.symbol').attr('src', '<?php bloginfo('template_directory'); ?>/assets/img/Ikony/<?php echo $pr->post_title; ?>-green.svg');
                    });

                    $('.hover-<?php echo $pr->ID; ?>').on('mouseleave', function() {
                        $(this).find('.symbol').attr('src', '<?php echo get_the_post_thumbnail_url($pr->ID); ?>');
                    });
                </script>
            <?php } ?>
        </div>
    </div>

    <!-- KONIEC KONTAINERA       -->




    <!--     POPULARNE PREPARATY KARUZELA     -->
    <div class="container-fluid flicks">
        <div class="ozdobniki">
            <!-- Block parent element -->
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/ozdobnik1.png" class="ozdobnik">
            <?php if (DefaultHelper::checkEn() == 'en') { ?>
                <p class="ozdobnik-inside text-center font-weight-bold button-up" style="background: transparent">POPULAR <br> PRODUCTS</p>
            <?php } else { ?>
                <p class="ozdobnik-inside text-center font-weight-bold button-up" style="background: transparent">POPULARNE <br> PREPARATY</p>
            <?php } ?>
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/popularne-preparaty.png" class="ozdobnik-inside">
        </div>

        <div class="row products-carousel">
            <div class="container ">
                <div class="row mt-5 w-100 mb-5 mr-0 ml-0">
                    <div class="w-100" data-flickity='{"groupCells":true,"imagesLoaded":true, "arrowShape": "M 12.881 47.156 L 41.738 20.164 L 48.024 26.125 L 32.959 41.407 L 84.983 41.407 L 84.983 52.571 L 32.923 52.636 L 47.184 65.096 L 42.005 72.977 Z" ,"autoPlay": 4500,  "prevNextButtons": true, "pageDots": false,"contain":true }'>
                        <?php while ($products->have_posts()) : $products->the_post(); ?>
                            <div class="carousel-cell col-xl-3 col-lg-4 col-md-4 col-sm-12 pt-3 pb-2 ">
                                <a href="<?php echo get_the_permalink($kat_prod); ?>">
                                    <div class="card h-100 align-items-center" <?php if (get_field('nowosc')) {
                                                                                    echo $checkRamka;
                                                                                }  ?>>
                                        <?php if (get_field('nowosc')) { ?>
                                            <a href="<?php echo get_the_permalink($kat_prod); ?>">
                                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                                    <img src="<?php bloginfo('template_directory'); ?>/assets/img/new@2.png" class="nowosc" /></a>
                                        <?php } else { ?>
                                            <img src="<?php bloginfo('template_directory'); ?>/assets/img/nowosc@2.png" class="nowosc" />
                                </a>
                            <?php } ?>
                        <?php } ?>
                        <div class="card-img-top d-flex align-items-end justify-content-center">
                            <img style=" max-width:100%;    max-height: 300px;" src="<?php the_post_thumbnail_url('large'); ?>" />
                        </div>
                        <div class="card-body flex-column d-flex">
                            <a href="<?php echo get_the_permalink($kat_prod); ?>">
                                <h5 class="card-title"><?php the_title(); ?></h5>
                            </a>
                            <a class="popular-text" href="<?php echo get_the_permalink($kat_prod); ?>">
                                <p><?php the_excerpt(); ?></p>
                            </a>
                            <div class="align-self-center w-75 mt-auto button-info align-self-conter pt-1 pb-1">
                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                    <?php if (get_field('link')) { ?>
                                        <a href="<?php echo get_field("link"); ?>" target="_blank" class="btn btn-success font-weight-bold mb-1 mt-1 pt-2 pb-2 align-self-center">Buy on-line</a>
                                    <?php } else { ?>
                                        <a href="https://ktomalek.pl" target="_blank" class="btn btn-success font-weight-bold mb-1 mt-1 pt-2 pb-2 align-self-center">Check availabity</a>
                                    <?php } ?>
                                    <a href="<?php echo get_the_permalink($kat_prod); ?>" class="btn btn-primary font-weight-bold mt-2 mb-2 pt-2 pb-2 align-self-center">Check It Out</a>
                                <?php } else { ?>
                                    <?php if (get_field('link')) { ?>
                                        <a href="<?php echo get_field("link"); ?>" target="_blank" class="btn btn-success font-weight-bold mb-1 mt-1 pt-2 pb-2 align-self-center">Kup on-line</a>
                                    <?php } else { ?>
                                        <a href="https://ktomalek.pl" target="_blank" class="btn btn-success font-weight-bold mb-1 mt-1 pt-2 pb-2 align-self-center">Sprawdź dostępnosć</a>
                                    <?php } ?>
                                    <a href="<?php echo get_the_permalink($kat_prod); ?>" class="btn btn-primary font-weight-bold mt-2 mb-2 pt-2 pb-2 align-self-center">Zobacz preparat</a>
                                <?php } ?>
                            </div>
                        </div>
                            </div>
                            </a>
                    </div>
                <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="ozdobniki">
        <img src="<?php bloginfo('template_directory'); ?>/assets/img/ozdobnik2.png" class="ozdobnik2">
        <a href="<?php echo site_url('/preparaty/') ?>" class="btn btn-primary ozdobnik-bottom  mt-1"><img src="<?php bloginfo('template_directory'); ?>/assets/img/wszystkie-preparaty@2.png"></a>
        <?php if (DefaultHelper::checkEn() == 'en') { ?>
            <h5 class="ozdobnik-inside text-down text-center mt-5 button-down">ALL <br> PRODUCTS</h5>
        <?php } else { ?>
            <h5 class="ozdobnik-inside text-down text-center mt-5 button-down">WSZYSTKIE <br> PREPARATY</h5>
        <?php } ?>
    </div>
    </div>


    <!--       AKTUALNOŚCI   -->

    <div class="container news mt-5 h-100">
        <?php if (DefaultHelper::checkEn() == 'en') { ?>
            <h2 class="text-center mb-5 font-weight-bold">News</h2>
        <?php } else { ?>
            <h2 class="text-center mb-5 font-weight-bold">Aktualności</h2>
        <?php } ?>
        <div class="row d-md-block">
            <?php while ($firstProduct->have_posts()) : $firstProduct->the_post(); ?>
                <div class="col-lg-6 col-md-12 col-sm-12 lewy">
                    <img class="card-img-top relative" src="<?php the_post_thumbnail_url('full'); ?>" alt="Card image cap" />
                    <a href="<?php the_permalink(); ?>">
                        <p class="title "><?php the_title(); ?></p>
                    </a>
                    <a href="<?php the_permalink(); ?>">
                        <p class="title"><?php the_excerpt($post); ?></p>
                    </a>
                </div>
            <?php endwhile; ?>

            <?php while ($news->have_posts()) : $news->the_post(); ?>
                <div class="col-lg-6 mb-5 d-flex prawy">
                    <div class="position-relative">
                        <a href="<?php the_permalink(); ?>"><img class="card-img-top" src="<?php the_post_thumbnail_url('full'); ?>" alt="Card image cap"></a>
                    </div>
                    <div class="card-news-text">
                        <a href="<?php the_permalink(); ?>">
                            <h5 class="card-title"><?php the_title(); ?></h5>
                        </a>
                        <hr>
                        <?php the_excerpt($post); ?>
                    </div>
                </div>

            <?php endwhile; ?>
        </div>
    </div>

    <div style="clear: both"></div>
    <!--      STREFA PACJENTA     -->
    <div class="container-strefa">
        <div class="container mt-5 text-center strefa-container">
            <?php if (DefaultHelper::checkEn() == 'en') { ?>
                <h3 class="font-weight-bold pt-5 strefa-title">Patient area</h3>
            <?php } else { ?>
                <h3 class="font-weight-bold pt-5 strefa-title">Strefa pacjenta</h3>
            <?php } ?>
        </div>
        <div class="container strefa">
            <div class="row w-100" style="margin: 0 auto !important;">
                <div class="carousel strefa-karuzela w-100 " data-flickity='{"imagesLoaded":true, "arrowShape": "M 12.881 47.156 L 41.738 20.164 L 48.024 26.125 L 32.959 41.407 L 84.983 41.407 L 84.983 52.571 L 32.923 52.636 L 47.184 65.096 L 42.005 72.977 Z" , "groupCells": true, "prevNextButtons": true, "pageDots": false, "freeScroll":true, "contain":true, "wrapAround":true }'>
                    <?php foreach ($postsStrefa as $pS) { ?>

                        <?php $all = get_field("kat_wpis", $pS->ID); ?>
                        <?php $color = get_field("patient_color", $pS->ID); ?>
                        <a style="text-decoration: none;" href="<?php echo get_the_permalink($pS); ?>">
                            <div class="carousel-cell col-lg-4 col-md-6 col-sm-12">
                                <div class="card" style="border: 1px solid rgba(131, 255, 20, 0.125);">
                                    <img class="ozdobnik-main" src="<?php bloginfo('template_directory') ?>/assets/img/<?php echo $color ?>">


                                    <img class="card-img-top rounded" src="<?php echo get_the_post_thumbnail_url($pS->ID); ?>" alt="Card image cap">
                                    <div class="card-body">
                                        <h4 class="card-title"><?php echo get_the_title($pS); ?></h4>
                                        <p class="card-text"><?php echo get_the_excerpt($pS); ?></p>
                                        <div class="container icon-container">
                                            <div class="row gridowe-ikonki">
                                                <?php if ($all) {
                                                    foreach ($all as $kat_prod) { ?>
                                                        <div class="col">
                                                            <img class="filter-color w-100" src="<?php bloginfo('template_directory') ?>/assets/img/Ikony/<?php echo get_cat_name($kat_prod); ?>-green.svg" />
                                                        </div>

                                                <?php }
                                                } ?>
                                            </div>
                                        </div>
                                        <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                            <a href="<?php echo get_the_permalink($pS); ?>" class="btn btn-primary mt-auto button-info" style=" text-decoration: none;    position: absolute;
    bottom: 20px;
    right: 13%;">Read more...</a>
                                        <?php } else { ?>
                                            <a href="<?php echo get_the_permalink($pS); ?>" class="btn btn-primary mt-auto button-info" style=" text-decoration: none;    position: absolute;
    bottom: 20px;
    right: 13%;">Dowiedz się więcej</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </a>

                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <!--      WSPÓłPRACA     -->

    <div class="coop container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 kolumienka-mobile">
                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                    <p class="main">Are you a manufacturer, pharmacy or pharmaceutical wholesaler?<br><br>
                        <b>We invite you to cooperate with us!</b>
                    </p> <?php } else { ?>
                    <p class="main">Jesteś producentem, apteką lub hurtownią farmaceutyczną?<br><br>
                        <b>Nawiąż z nami współpracę!</b>
                    </p>
                <?php } ?>
                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                    <div>
                        <a href="/en/cooperation/" class="btn btn-primary mt-5 btn-wspolpraca">More about cooperate</a>
                    </div>
                <?php } else { ?>
                    <div>
                        <a href="/wspolpraca" class="btn btn-primary mt-5 btn-wspolpraca">Więcej o współpracy</a>
                    </div>
                <?php } ?>
            </div>
            <div class="col">
            </div>
        </div>
    </div>


    <!--    Kariera     -->

    <div class="container-fluid carrier">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 kolumienka-mobile">
                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                    <p class="main"><b>Career</b></p>
                    <small class="main">Get to know us and learn more about working at Vitamed Pharmaceuticals</small>
                    <div>
                        <a href="/en/career/" class="btn btn-primary btn-wspolpraca mt-5">More about career</a>
                    </div>
                <?php } else { ?>
                    <p class="main"><b>Kariera</b></p>
                    <small class="main">Poznaj nas i dowiedz się więcej o pracy w Vitamed Pharmaceuticals</small>
                    <div>
                        <a href="/kariera" class="btn btn-primary btn-wspolpraca mt-5">Więcej o współpracy</a>
                    </div>
                <?php } ?>
            </div>
            <div class="col">
            </div>
        </div>
    </div>

</main><!-- /.container -->

<style>
    @media (min-resolution: 1.5dppx) and (min-width: 1000px) {
        .header {
            height: 800px !important;
        }

        .header-mask {
            height: 800px !important;
        }
    }
</style>

<?php get_footer();
