<?php
/*
 * Template Name: Produkt
 * Template Post Type: post, page, product
 */
$productsHome = ProductsService::getPagesByTemplate('products.php');
get_header();  ?>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/style.css" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/single-product.css" />

<style>
    @media (max-width: 767px) {
        .header {
            height: 420px !important;
        }
        .header-mask {
            height: 420px !important;
        }
    }
    @media (max-width: 480px)
    {
        .title-container {
            margin-top: 8rem;
            text-align: center;
        }
        .header-title-bold-black{
          font-size: 2.9rem !important;
        }
    }

</style>
<?php while (have_posts()) : the_post(); ?>
    <main role="main" class="w-100">
        <div class="container">
            <div class="row">
                <div class="col content margin-top">
                    <?php the_content(); ?>
                </div>

                <div class="col right d-flex justify-content-center">
                    <div class="div-float-helper">
                        <div class="w-100 box">
                            <div class="image-cropper">
                                <img class="niebieski-ozdobnik" src="<?php bloginfo('template_directory'); ?>/assets/img/ozdobnik3.png">
                                <div class="rounded-image" style="background-image: url('<?php bloginfo('template_directory'); ?>/assets/img/symbol-dawkowanie@2.png');"></div>
                                <div class="description">
                                    <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                        <h3>Dosage</h3>
                                        <p><?php echo get_field('dawkowanie'); ?></p>
                                    <?php } else { ?>
                                        <h3>Dawkowanie</h3>
                                        <p><?php echo get_field('dawkowanie'); ?></p>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="image-cropper-alt no-border">
                                <img class="niebieski-ozdobnik" src="<?php bloginfo('template_directory'); ?>/assets/img/ozdobnik3.png" />
                                <div class="rounded-image" style="background-image: url('<?php bloginfo('template_directory'); ?>/assets/img/symbol-sklad@2.png');"></div>
                                <div class="description">
                                    <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                        <h3>Active substance</h3>
                                        <p><?php echo get_field('substancje'); ?></p>
                                    <?php } else { ?>
                                        <h3>Substancje czynne</h3>
                                        <p><?php echo get_field('substancje'); ?></p>
                                    <?php } ?>
                                </div>

                            </div>
                            <div class="wyrob">
                                <p class="pt-2"><?php echo get_field('wyrob'); ?></p>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
        <div class="container-fluid  wskazania">
            <div class="container">
                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                    <h3>Indications</h3>
                    <p><?php echo get_field('wskazania'); ?></p>
                <?php } else { ?>
                    <h3>Wskazania</h3>
                    <p><?php echo get_field('wskazania'); ?></p>
                <?php } ?>
            </div>
        </div>
        <div class="container dodatkowy mb-5">
            <p class="dodatkowy-tekst"><?php echo get_field('opis_lek'); ?></p>
        </div>
        <div class="container-fluid preparaty">
            <h3>Zobacz pozostałe preparaty</h3>

            <div class="row text-center preparaty-row">
                <?php foreach ($productsHome as $pr) { ?>
                    <a class="arrow order-<?php echo $pr->ID; ?>" style="color:black" href="<?php echo $pr->guid; ?>">
                        <div class="card-offer col hover-<?php echo $pr->ID; ?>">
                            <img class="symbol" src="<?php echo get_the_post_thumbnail_url($pr->ID); ?>">
                            <p class="commons"><?php echo $pr->post_title; ?></p>
                            <button class="btn btn-primary btn-offer">
                                <img class="w-50" src="<?php bloginfo('template_directory'); ?>/assets/img/strzalka4@2.png">
                            </button>
                        </div>
                    </a>
                    <script>
                        $('.hover-<?php echo $pr->ID; ?>').on('mouseover', function() {
                            $(this).find('.symbol').attr('src', '<?php bloginfo('template_directory'); ?>/assets/img/Ikony/<?php echo $pr->post_title; ?>-green.svg');
                        });

                        $('.hover-<?php echo $pr->ID; ?>').on('mouseleave', function() {
                            $(this).find('.symbol').attr('src', '<?php echo get_the_post_thumbnail_url($pr->ID); ?>');
                        });
                    </script>
                <?php } ?>
            </div>
        </div>
    </main>

    <style>
        @media (min-resolution: 2dppx) and (min-width: 1024px) {
            .header-image {
                height: 80vh !important;
            }
        }

        @media (min-resolution: 1.5dppx) and (min-width: 1000px) {
            .header-image {
                height: 110vh;
            }
        }
    </style>
<?php endwhile; ?>
<script>
    function productMoveOnMobile() {
        var foo = `<?php bloginfo('template_directory'); ?>`;
        console.log(foo);
        if (window.innerWidth < 768) {
            $(".image-container").remove();
            $('.content').prepend(` <div class="" style="width:100%;  text-align: center;    position: static;">
    <?php if (get_field('nowosc')) { ?>
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/img/nowosc@2.png" class="nowosc" />
                                    <?php } ?>
     
                        <img class="product-image" style="width:100%; height: auto;"src="<?php echo the_post_thumbnail_url('full'); ?>"/>
                 
    </div>`);
        }
    };
    productMoveOnMobile();
</script>
<?php get_footer();
