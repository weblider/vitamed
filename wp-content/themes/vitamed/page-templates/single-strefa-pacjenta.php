<?php
/*
 * Template Name: Strefa pacjenta
 * Template Post Type: post, page, product
 */
get_header();
$products = ProductsService::getProducts(null, -1);
$postsStrefa = ProductsService::getProducts(null, -1, 'Strefa Pacjenta')->posts;
$patientNews = get_field("patient_news");
$page_id = get_queried_object_id();


?>
<style>
    .container {
        max-width: 1400px !important;
    }
</style>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/single-strefa.css" />
<style>
    @media (max-width: 480px) {

        .title-container {
            margin-top: 10rem;
            text-align: center;
        }

    }
</style>

<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>


    <body class="body">
        <main class="main">
            <div class="container-fluid ">
                <div class="container mt-5 mb-5">
                    <div class="row">
                        <div class="col-lg-8 col-md-12 col-sm-12">
                            <?php the_content(); ?>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12 pr-sm-0 pl-sm-0 pr-lg-4 pl-lg-4 ">
                            <div class="carousel w-100  " data-flickity='{"pageDots": false, "cellAlign": "left", "adaptiveHeight": false, "imagesLoaded":true, "arrowShape": "M 12.881 47.156 L 41.738 20.164 L 48.024 26.125 L 32.959 41.407 L 84.983 41.407 L 84.983 52.571 L 32.923 52.636 L 47.184 65.096 L 42.005 72.977 Z" , "groupCells": true, "prevNextButtons": true, "pageDots": false, "freeScroll":true, "contain":true, "wrapAround":false }'>
                                <?php if ($patientNews) { ?>
                                    <?php foreach ($patientNews as $patient) { ?>
                                        <?php $all = get_field("kat_wpis", $patient->ID); ?>
                                        <?php $color = get_field("patient_color", $patient->ID); ?>
                                        <a style="text-decoration: none;" href="<?php echo get_the_permalink($patient); ?>">
                                            <div class="carousel-cell col-12  p-lg-5">
                                                <div class="card mt-5 card-round" style="border: 1px solid rgba(248,2,85,0.125); ">
                                                    <img class="ozdobnik-main position-absolute w-100" src="<?php bloginfo('template_directory') ?>/assets/img/<?php echo $color ?>">
                                                    <img class="card-img-top round-image" src="<?php echo get_the_post_thumbnail_url($patient->ID); ?>">
                                                    <div class="card-body pt-3 pb-3 h-100 text-center">
                                                        <h5 class="card-title"><?php echo get_the_title($patient->ID); ?></h5>
                                                        <p class="card-text"><?php echo get_the_excerpt($patient->ID); ?></p>
                                                        <div class="row align-content-center">
                                                            <?php if ($all) {
                                                                foreach ($all as $kat_prod) { ?>
                                                                    <div class="pic col-4 pt-5 align-self-center">
                                                                        <img class="filter-color p-md-5 p-lg-1 p-sm-5 w-75 align-self-center " src="<?php bloginfo('template_directory') ?>/assets/img/Ikony/<?php echo get_cat_name($kat_prod); ?>-green.svg" />
                                                                    </div>
                                                            <?php }
                                                            } ?>
                                                        </div>
                                                    </div>
                                                    <a href="<?php echo get_the_permalink($patient); ?>" class="btn btn-primary mt-auto button-info align-self-center w-50">Dowiedz się więcej</a>
                                                </div>


                                            </div>
                                        </a>

                                    <?php  } ?>
                                <?php  } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!--            --><?php //if (get_field("prod_wpis", $post)){ 
                                    ?>
                <div class="row products-carousel">
                    <div class="container ">
                        <div class="row mt-5 w-100 mb-5 mr-0 ml-0">
                            <div class="w-100" data-flickity='{"groupCells":true,"imagesLoaded":true, "arrowShape": "M 12.881 47.156 L 41.738 20.164 L 48.024 26.125 L 32.959 41.407 L 84.983 41.407 L 84.983 52.571 L 32.923 52.636 L 47.184 65.096 L 42.005 72.977 Z" ,"autoPlay": 4500,  "prevNextButtons": true, "pageDots": false,"contain":true, "freeScroll": true, "wrapAround":true}'>
                                <?php $allProducts = get_field("prod_wpis", $post); ?>
                                <?php if ($allProducts) {
                                    foreach ($allProducts as $kat_prod) { ?>
                                        <a style="text-decoration: none;" href="<?php echo get_the_permalink($kat_prod); ?>">
                                            <div class="carousel-cell col-lg-3 col-md-4 col-sm-12 p-2">

                                                <div class="card h-100">
                                                    <img class="card-img-top" style="height: 300px; width: auto !important;" src="<?php echo get_the_post_thumbnail_url($kat_prod) ?>">
                                                    <div class=" card-body h-100 flex-column d-flex">
                                                        <h5><?php echo get_the_title($kat_prod); ?></h5>
                                                        <p><?php echo get_the_excerpt($kat_prod); ?></p>
                                                        <a href="<?php echo get_the_permalink($kat_prod); ?>" class="btn btn-primary mt-auto button-info align-self-center w-50 mt-auto font-weight-bold" style="margin-top: 20px;">Sprawdź</a>
                                                    </div>

                                                </div>
                                        </a>
                            </div>
                    <?php }
                                } ?>
                        </div>
                    </div>
                </div>
            </div>
            <!--            --><?php //} 
                                ?>
            </div>
        </main>
    </body>
<?php endwhile; ?>

<?php get_footer();
