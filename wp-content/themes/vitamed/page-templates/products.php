<?php
/*
 * Template Name: Produkty
 */

get_header();

$products = ProductsService::getByCategory(0, -1, get_the_title());
$count = $products->post_count;

?>


<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/products.css" />

<main role="main" class="w-100">
    <div class="container">
        <div class="row next-section">
            <div class="col">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
    <!--     POPULARNE PREPARATY KARUZELA     -->
    <div class="container-fluid flicks">
        <div class="ozdobniki">
            <!-- Block parent element -->
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/ozdobnik1.png" class="ozdobnik">
            <?php if (DefaultHelper::checkEn() == 'en') { ?>
                <p class="ozdobnik-inside text-center font-weight-bold button-up" style="background: transparent">POPULAR <br> PRODUCTS</p>
            <?php } else { ?>
                <p class="ozdobnik-inside text-center font-weight-bold button-up" style="background: transparent">POPULARNE <br> PREPARATY</p>
            <?php } ?>
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/popularne-preparaty.png" class="ozdobnik-inside">
        </div>

        <div class="row products-carousel">
            <div class="container ">
                <div class="row w-100 mb-5 mr-0 ml-0">
                    <?php $produkty = get_field('products_category'); ?>
                    <?php if ($produkty) { ?>
                        <?php foreach ($produkty as $produkt) { ?>
                            <a href="<?php echo get_the_permalink($produkt->ID); ?>" style="text-decoration: none">
                                <div class="carousel-cell col-xl-3 col-lg-4 col-md-4 col-sm-12 pt-1 pb-1 mt-4 mb-5 ">
                                    <div class="card h-100 align-items-center" <?php if (get_field('nowosc')) {
                                                                                    echo $checkRamka;
                                                                                }  ?>>
                                        <?php if (get_field('nowosc')) { ?>
                                            <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                                <img src="<?php bloginfo('template_directory'); ?>/assets/img/new@2.png" style="transform: translate(14%, -15px) !important;" class="nowosc" />
                                            <?php } else { ?>
                                                <img src="<?php bloginfo('template_directory'); ?>/assets/img/nowosc@2.png" style="transform: translate(14%, -15px) !important;" class="nowosc" />
                                            <?php } ?>
                                        <?php } ?>
                                        <div class="card-img-top d-flex align-items-end justify-content-center">
                                            <img class="product-category-thumbnail" style=" max-width:100%;   max-height: 300px;" src="<?php echo get_the_post_thumbnail_url($produkt->ID); ?>" />
                                        </div>
                                        <div class="card-body flex-column d-flex">
                                            <h5 class="card-title"><?php echo get_the_title($produkt->ID); ?></h5>
                                            <p><?php echo get_the_excerpt($produkt->ID); ?></p>
                                            <div class="align-self-center w-75 mt-auto button-info align-self-conter pt-1 pb-1">
                                                <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                                    <?php if (get_field('link', $produkt->ID)) { ?>
                                                        <a href="<?php echo get_field("link", $produkt->ID); ?>" target="_blank" class="btn btn-success font-weight-bold mb-1 mt-1 pt-2 pb-2 align-self-center">Buy on-line</a>
                                                    <?php } else { ?>
                                                        <a href="https://ktomalek.pl" target="_blank" class="btn btn-success font-weight-bold mb-1 mt-1 pt-2 pb-2 align-self-center">Check availabity</a>
                                                    <?php } ?>
                                                    <a href="<?php echo get_the_permalink($produkt->ID); ?>" class="btn btn-primary font-weight-bold mt-2 mb-2 pt-2 pb-2 align-self-center">Check It Out</a>
                                                <?php } else { ?>
                                                    <?php if (get_field('link', $produkt->ID)) { ?>
                                                        <a href="<?php echo get_field("link", $produkt->ID); ?>" target="_blank" class="btn btn-success font-weight-bold mb-1 mt-1 pt-2 pb-2 align-self-center">Kup on-line</a>
                                                    <?php } else { ?>
                                                        <a href="https://ktomalek.pl" target="_blank" class="btn btn-success font-weight-bold mb-1 mt-1 pt-2 pb-2 align-self-center">Sprawdź dostępność</a>
                                                    <?php } ?> <a href="<?php echo get_the_permalink($produkt->ID); ?>" class="btn btn-primary font-weight-bold mt-2 mb-2 pt-2 pb-2 align-self-center">Zobacz preparat</a>
                                                <?php } ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                    <?php }
                    } ?>
                </div>
            </div>
        </div>

        <div class="ozdobniki">
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/ozdobnik2.png" class="ozdobnik2">
            <?php if (DefaultHelper::checkEn() == 'en') { ?>
                <a href="<?php echo site_url('/en/preparations/') ?>" class="btn btn-primary ozdobnik-bottom  mt-1">
                <?php } else { ?>
                    <a href="<?php echo site_url('/preparaty/') ?>" class="btn btn-primary ozdobnik-bottom  mt-1">
                    <?php } ?>

                    <img src="<?php bloginfo('template_directory'); ?>/assets/img/wszystkie-preparaty@2.png"></a>
                    <?php if (DefaultHelper::checkEn() == 'en') { ?>
                        <h5 class="ozdobnik-inside text-down text-center mt-5 button-down">ALL <br> PRODUCTS</h5>
                    <?php } else { ?>
                        <h5 class="ozdobnik-inside text-down text-center mt-5 button-down">WSZYSTKIE <br> PREPARATY</h5>
                    <?php } ?>
        </div>

    </div>
</main>

<?php get_footer();
