<?php
/*
 * Template Name: Strefa pacjenta
 */

get_header();
if (DefaultHelper::checkEn() == 'en'){
	$args = array(
		'post_type'=> 'post',
		'category_name' => 'Strefa pacjenta',
		'orderby'    => 'date',
		'post_status' => 'publish',
		'order'    => 'ASC',

		'language' => 'pl'
	);
	$result = new WP_Query( $args );
	$posts = $result;
}else{
	$posts = ProductsService::getStrefa(null,-1);

}


?>

    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/news.css"/>


    <main role="main" class="w-100">
        <div class="container mb-5 container-strefa-pacjenta-lista">
            <div class="row news-list mb-5">
				<?php while ( $posts->have_posts() ) : $posts->the_post(); ?>
                    <div class="col-lg-4 col-md-6 col-sm-12 mb-5" style="">
                        <a style="text-decoration: none; color: black" href="<?php the_permalink();?>">
                            <div class="card  d-table-cell align-middle h-100" style="width: 18rem;">
                                <div class="position-relative text-center">
                                    <img class="card-img-top" src="<?php the_post_thumbnail_url( 'medium' ); ?>" alt="Card image cap">
<!--                                    <div class="data">--><?php //echo get_the_date( 'd.m.Y' ); ?><!--</div>-->
                                </div>
                                <div class="card-body d-flex flex-column">
                                    <h5 class="card-title"><?php the_title(); ?></h5>
                                    <p class="card-text mt-auto"><?php the_excerpt($post); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
				<?php endwhile; ?>
            </div>

        </div>
    </main>

<?php get_footer();
