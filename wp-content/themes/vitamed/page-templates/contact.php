<?php
/*
 * Template Name: Kontakt
 */

get_header();


?>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/contact.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js" integrity="sha512-UwcC/iaz5ziHX7V6LjSKaXgCuRRqbTp1QHpbOJ4l1nw2/boCfZ2KlFIqBUA/uRVF0onbREnY9do8rM/uT/ilqw==" crossorigin="anonymous"></script>
<main role="main" class="w-100">
    <div class="container ">
        <?php if (DefaultHelper::checkEn() == 'en') { ?>
            <div class="col next-section">
                <div class="blok">
                    <p class="title">We are here</p>
                    <i class="fas fa-map-marker-alt"></i>
                    <div class="contact-wrapper" style="text-align: left">
                        <div class="pt-3">
                            <p>ul. Ostrowskiego 24 </p>
                            <p>97-213 Smardzewice</p>
                        </div>
                        <div class="pt-3">
                            <p>ul. Kędzierzyńska 17a</p>
                            <p>41-902 Bytom</p>
                        </div>
                        <div class="pt-3">
                            <p>Aleje Jerozolimskie 65/79</p>
                            <p> 00-697 Warszawa</p>
                        </div>
                    </div>

                </div>
                <div class="blok">
                    <p class="title">Contact us</p>
                    <i class="fas fa-phone"></i>
                    <div class="contact-wrapper">
                        <div class="mt-3">
                            <a href="tel:447108699">
                                <p>Phone: 44 710 86 99</p>
                            </a>
                        </div>
                        <div class="mt-3">
                            <a href="mailto:kontakt@vitamed.pl">
                                <p>Email: kontakt@vitamed.pl</p>
                            </a>
                        </div>
                    </div>

                </div>
                <div class="blok">
                    <p class="title">About out company</p>
                    <i class="fas fa-info"></i>
                    <div class="contact-wrapper">
                        <div class="pt-3">
                            <p>KRS: 0000846918</p>
                            <p>NIP: 7732493201</p>
                            <p>Regon: 386354055</p>
                            <p>Share capital: 1 000 000,00zł</p>
                            <p>Legal form: Limited liability company </p>
                            <p>District Court for Łódź-Śródmieście in Łódź Commercial Court, XX Commercial Division of the National Court Register</p>
                        </div>
                    </div>

                </div>
            </div>
        <?php } else { ?>
            <div class="col next-section">
                <div class="blok">
                    <p class="title">Jesteśmy tutaj</p>
                    <i class="fas fa-map-marker-alt"></i>
                    <div class="contact-wrapper" style="text-align: left">
                        <div class="pt-3">
                            <p>ul. Ostrowskiego 24 </p>
                            <p>97-213 Smardzewice</p>
                        </div>
                        <div class="pt-3">
                            <p>ul. Kędzierzyńska 17a</p>
                            <p>41-902 Bytom</p>
                        </div>
                        <div class="pt-3">
                            <p>Aleje Jerozolimskie 65/79</p>
                            <p> 00-697 Warszawa</p>
                        </div>
                    </div>

                </div>
                <div class="blok">
                    <?php if (DefaultHelper::checkEn() == 'en') { ?>
                        <p class="title">Contact with us</p>
                    <?php } else { ?>
                        <p class="title">Skontaktuj się z nami</p>
                    <?php } ?>

                    <i class="fas fa-phone"></i>
                    <div class="contact-wrapper">
                        <div class="mt-3">
                            <a href="tel:447108699">
                                <p>Tel: 44 710 86 99</p>
                            </a>
                        </div>
                        <div class="mt-3">
                            <a href="mailto:kontakt@vitamed.pl">
                                <p>Email: kontakt@vitamed.pl</p>
                            </a>
                        </div>
                    </div>

                </div>
                <div class="blok">
                    <p class="title">Dane firmy</p>
                    <i class="fas fa-info"></i>
                    <div class="contact-wrapper">
                        <div class="pt-3">
                            <p>KRS: 0000846918</p>
                            <p>NIP: 7732493201</p>
                            <p>Regon: 386354055</p>
                            <p>Kapitał zakładowy: 1 000 000,00zł</p>
                            <p>Forma prawna: Spółka z ograniczoną odpowiedzialnością </p>
                            <p>Sąd Rejonowy dla Łodzi-Śródmieścia w Łodzi Sąd Gospodarczy XX Wydział Gospodarczy Krajowego Rejestru Sądowego</p>
                        </div>
                    </div>

                </div>
            </div>
        <?php } ?>

        <div class="col position-relative">
            <div class="kariera ml-2">
                <div class="text-center mb-1">
                    <?php if (DefaultHelper::checkEn() == 'en') { ?>
                        <h4>Contact with us</h4>
                    <?php } else { ?>
                        <h4>Skontaktuj się z nami</h4>
                    <?php } ?>

                </div>
                <div role="form" class="wpcf7" id="wpcf7-f2096-o1" lang="pl-PL" dir="ltr">
                    <div class="screen-reader-response">
                        <p role="status" aria-live="polite" aria-atomic="true"></p>
                        <ul></ul>
                    </div>
                    <form action="/kontakt/#wpcf7-f2096-o1" id="validate" method="post" class="wpcf7-form init" data-status="init">
                        <div style="display: none;">
                            <input type="hidden" name="_wpcf7" value="2096">
                            <input type="hidden" name="_wpcf7_version" value="5.4">
                            <input type="hidden" name="_wpcf7_locale" value="pl_PL">
                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f2096-o1">
                            <input type="hidden" name="_wpcf7_container_post" value="0">
                            <input type="hidden" name="_wpcf7_posted_data_hash" value="">
                        </div>
                        <div class="form-group">
                            <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                <p><label class="w-100"> Name: *<br>
                                        <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" id="your-name" value="" size="40" class="form-control wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span> </label></p>
                            <?php } else { ?>
                                <p><label class="w-100"> Imię i nazwisko: *<br>
                                        <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" id="your-name" value="" size="40" class="form-control wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span> </label></p>
                            <?php } ?>

                        </div>
                        <div class="form-group">
                            <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                <p><label class="w-100"> E-mail: *<br>
                                        <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-email" id="your-email" value="" size="40" class="form-control wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span> </label></p>
                            <?php } else { ?>
                                <p><label class="w-100"> Adres e-mail: *<br>
                                        <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" id="your-email" value="" size="40" class="form-control wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span> </label></p>
                            <?php } ?>

                        </div>
                        <div class="form-group">
                            <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                <p><label class="w-100"> Phone: *<br>
                                        <span class="wpcf7-form-control-wrap tel"><input type="tel" name="tel" id="tel" value="" size="40" class="form-control wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false"></span> </label></p>
                            <?php } else { ?>
                                <p><label class="w-100"> Telefon: *<br>
                                        <span class="wpcf7-form-control-wrap tel"><input type="tel" name="tel" id="tel" value="" size="40" class="form-control wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false"></span> </label></p>
                            <?php } ?>

                        </div>
                        <div class="form-group">
                            <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                <p><label class="w-100"> Message: *<br>
                                        <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="form-control wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></textarea></span> </label></p>
                            <?php } else { ?>
                                <p><label class="w-100"> Wiadomość: *<br>
                                        <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="form-control wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></textarea></span> </label></p>
                            <?php } ?>


                        </div>
                        <div class="form-group">
                            <p><label class="w-100">
                                    <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                        <label><span class="wpcf7-form-control-wrap acceptance"><span class="wpcf7-form-control wpcf7-acceptance"><span class="wpcf7-list-item"><label><input type="checkbox" name="acceptance" value="1" aria-invalid="false"><span class="wpcf7-list-item-label">I agree to have my personal data processed by VITAMED PHARMACEUTICALS Sp. z o.o. *</span></label></span></span></span><br>
                                        <?php } else { ?>
                                            <label><span class="wpcf7-form-control-wrap acceptance"><span class="wpcf7-form-control wpcf7-acceptance"><span class="wpcf7-list-item"><label><input type="checkbox" name="acceptance" value="1" id="my-checkbox" aria-invalid="false"><span class="wpcf7-list-item-label">Wyrażam zgodę na przetwarzanie danych osobowych przez VITAMED PHARMACEUTICALS Sp. z o. o. *</span></label></span></span></span><br>
                                            </label>
                                        <?php } ?>


                                        <span class="wpcf7-form-control-wrap checkbox"><span class="wpcf7-form-control wpcf7-checkbox wpcf7-validates-as-required"></span></span><br>
                                        </label></p>
                        </div>
                        <div class="form-group">
                            <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                <p class="btn-text"><input type="submit" value="SUBMIT" class="wpcf7-form-control wpcf7-submit w-50"></p>
                            <?php } else { ?>
                                <p class="btn-text"><input type="submit" value="WYŚLIJ" class="wpcf7-form-control wpcf7-submit w-50"></p>
                            <?php } ?>
                    </form>
                </div>
                <div class="alert alert-success" style="display: none;" role="alert">
                    <?php if (DefaultHelper::checkEn() == 'en') { ?>
                        Form was sent
                    <?php } else { ?>
                        Formularz wysłany
                    <?php } ?>
                </div>
                <div class="alert alert-danger" style="display: none;" role="alert">
                    <?php if (DefaultHelper::checkEn() == 'en') { ?>
                        Fill all empty inputs!
                    <?php } else { ?>
                        Uzupełnij wszystkie dane!
                    <?php } ?>
                </div>
                <p>* - pola wymagane</p>
                <!-- <form>
                    <div class="form-group">
                        <label for="username">Imię i nazwisko: *</label>
                        <input type="username" class="form-control" id="username" placeholder="Jan Kowalski" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Adres e-mail: * </label>
                        <input type="email" class="form-control" id="email" placeholder="Wpisz e-mail" required>
                    </div>
                    <div class="form-group">
                        <label for="phone">Telefon: *</label>
                        <input type="text" class="form-control" id="phone" placeholder="Wpisz numer telefonu" required>
                    </div>
                    <div class="form-group">
                        <label for="message">Wiadomość: *</label>
                        <textarea type="" class="form-control" id="message" placeholder="Wpisz wiadomość" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-success w-100 mt-5">WYŚLIJ</button>
                    <small class="pt-2">* - pola obowiązkowe</small>
                </form> -->
            </div>
        </div>
        <script>
            function sleep(milliseconds) {
                const date = Date.now();
                let currentDate = null;
                do {
                    currentDate = Date.now();
                } while (currentDate - date < milliseconds);
            }
        </script>
        <script>
            console.log("asD");
            $("#validate").submit(function() {
                if (document.getElementById("tel").value.length == 0 || document.getElementById("your-email").value.length == 0 || document.getElementById('your-name').value.length == 0) {
                    $('.alert-danger').css("display", "block");
                    return false;
                } else {
                    $('.alert-danger').css("display", "none");
                    $('.alert-success').css("display", "block");
                    sleep(2000);
                    return true;
                }
            });
        </script>
</main>

<?php get_footer();
