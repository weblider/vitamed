<?php
$productsHome = ProductsService::getPagesByTemplate('products.php');
$postsStrefa = ProductsService::getStrefa(null, 5);

require 'page-templates/misc/partners.php';
?>
<script>
    if (window.devicePixelRatio < 1.5 && window.innerWidth > 1000) {

        window.sr = ScrollReveal();
        sr.reveal('.lewy', {
            distance: '150%',
            origin: 'left',
            opacity: null,
            delay: 100,
            duration: 500
        });


        //
        // window.sr = ScrollReveal();
        // sr.reveal('.col-4', {
        //     distance: '150%',
        //     origin: 'left',
        //     opacity: null,
        //     delay: 100,
        //     duration: 500
        // });


        window.sr = ScrollReveal();
        sr.reveal('.prawy', {
            distance: '150%',
            origin: 'bottom',
            opacity: null,
            delay: 100,
            duration: 500
        });

        window.sr = ScrollReveal();
        sr.reveal('.right', {
            distance: '150%',
            origin: 'bottom',
            opacity: null,
            delay: 0,
            duration: 900
        });
        window.sr = ScrollReveal();
        sr.reveal('.kolumienka-mobile', {
            distance: '150%',
            origin: 'left',
            opacity: null,
            delay: 200,
            duration: 900
        });

        sr.reveal('.content', {
            distance: '150%',
            origin: 'bottom',
            opacity: null,
            delay: 100,
            duration: 900
        });
        sr.reveal('.naglowek-napis', {
            distance: '150%',
            origin: 'left',
            opacity: null,
            delay: 200,
            duration: 900
        });


    } else {
        window.sr = ScrollReveal();

        sr.reveal('.naglowek-napis', {
            distance: '150%',
            origin: 'left',
            opacity: null,
            delay: 200,
            duration: 900
        });
    }
</script>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/footer.css" />


<style>
    .flickity-button-icon {
        -webkit-box-shadow: 0px 12px 46px -1px rgba(0, 0, 0, 0.75);
        -moz-box-shadow: 0px 12px 46px -1px rgba(0, 0, 0, 0.75);
        box-shadow: 0px 12px 46px -1px rgba(0, 0, 0, 0.75);
        border-radius: 50%;
    }
</style>

<div class="container-fluid" style="background-color: #009fe3">
    <div class="container ">
        <div class="footer">
            <div class="row">
                <div class="col-sm-6 col-md-3 footer-col info">
                    <strong>
                        VITAMED PHARMACEUTICALS Sp. z o.o.
                    </strong>
                    <br>
                    <p>
                        97-213 Smardzewice<br>
                        ul. Ostrowskiego 24<br>
                        Tel/Fax <a href="tel:+48447108699">(+48) 44 710 86 99</a><br>
                        <a href="mailto:kontakt@vitamed.pl">
                            <b style="color: white;">kontakt@vitamed.pl</b>
                        </a>
                    </p>
                    <a href="<?php echo site_url('/polityka-prywatnosci/') ?>">
                        <p>
                            <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                <b style="color: white;">Privacy policy</b>
                            <?php } else { ?>
                                <b style="color: white;">Polityka prywatności</b>
                            <?php } ?>

                        </p>
                    </a>

                    <a href="<?php echo site_url('/polityka-cookies/') ?>">
                        <p>
                            <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                <b style="color: white;">Cookie policy</b>
                            <?php } else { ?>
                                <b style="color: white;">Polityka cookies</b>
                            <?php } ?>
                        </p>
                    </a>

                </div>
                <div class="col-sm-6 col-md-2 footer-col list-container ">
                    <?php if (DefaultHelper::checkEn() == 'en') { ?>
                        <h4>Products</h4>
                    <?php } else { ?>
                        <h4>Produkty</h4>
                    <?php } ?>
                    <nav>
                        <ul>
                            <?php foreach ($productsHome as $pr) { ?>
                                <li><a href="/<?php echo $pr->post_title; ?>"><?php echo $pr->post_title; ?></a></li>
                            <?php } ?>
                        </ul>
                    </nav>
                </div>
                <div class="col-sm-6 col-md-4 footer-col list-container ">
                    <?php if (DefaultHelper::checkEn() == 'en') { ?>
                        <h4>Patient area</h4>
                    <?php } else { ?>
                        <h4>Strefa Pacjenta</h4>
                    <?php } ?>
                    <nav class="socials-navigation">
                        <ul>
                            <?php foreach ($postsStrefa->posts as $pS) { ?>
                                <li><a href="/strefa-pacjenta/<?php echo $pS->post_name; ?>"><?php echo $pS->post_title; ?></a></li>
                            <?php } ?>
                        </ul>
                    </nav>
                </div>
                <div class="col-sm-6 col-md-3 footer-col list-container" style="margin-top: -2%">
                    <div class="card">
                        <div class="card-body">
                            <?php if (DefaultHelper::checkEn() == 'en') { ?>
                                <h5 class="card-title">Where to buy</h5>
                                <p class="card-text">All our products are available on-line in the store:</p>
                            <?php } else { ?>
                                <h5 class="card-title">Gdzie kupić</h5>
                                <p class="card-text">Wszystkie nasze preparaty są do nabycia on-line w sklepie:</p>
                            <?php } ?>

                            <a href="https://i-pharma24.pl" style="cursor: pointer;" target="_blank"><input type="text" style="cursor: pointer; padding:10px 10px 10px 10px; background-color: white; border: none; color: black; font-weight: 600; border-bottom-left-radius: 15px;
                                   border-bottom-right-radius: 15px;
                                   border-top-right-radius: 15px;" class="w-100 text-center" value="www.i-pharma24.pl" readonly></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </div>
    <!-- <div class="cookie-policy-bar">
        <p class="cookie-policy-text">Korzystając z naszej strony, zgadzasz się na zapisywanie plików cookies</p>
        <div class="cookie-policy-wrapper">
            <a class="cookie-policy-link" href="<?php echo site_url('/polityka-prywatnosci/') ?>">Więcej informacji w polityce prywatności</a>
            <button class="btn cookie-policy-button">Akceptuję</button>
        </div>
    </div> -->

    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/js/script.js"></script>

    <!-- TO CHWILOWO, NAPRAWIĆ -->
    <!-- <?php if (is_page('home')) : ?> -->

    <!-- <?php else : ?>
            <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/js/pages.js"></script>
    <?php endif; ?> -->
</div>
<?php $products = ProductsService::getProducts(null, -1, 'Produkty'); ?>
<style>
    .karuzela-efekt .flickity-viewport {
        height: 550px !important;
    }
</style>
<script>
    if (window.innerWidth < 1100) {
        $(".for-carousel").prepend(`<div class=" karuzela-efekt" style="height: 500px;text-align: center; width: 100%;">
    <div class="karuzela ">
        <div class="carousel pyk" data-flickity='{"imagesLoaded":true, "arrowShape": "M 12.881 47.156 L 41.738 20.164 L 48.024 26.125 L 32.959 41.407 L 84.983 41.407 L 84.983 52.571 L 32.923 52.636 L 47.184 65.096 L 42.005 72.977 Z" , "groupCells": true, "autoPlay": true, "prevNextButtons": true, "pageDots": false, "freeScroll":false, "contain":true, "wrapAround":true } '>
		<?php while ($products->have_posts()) : $products->the_post(); ?>

        <div class="carousel-cell  h-100 col">
        <a href="<?php the_permalink(); ?>" style="text-decoration: none;">
        <div class="card-img-top d-flex align-items-center justify-content-center img-carosell" style="height: 70%;">
        <img class="" src="<?php the_post_thumbnail_url('medium'); ?>" />
        </div>
        <h5 class="card-title font-weight-bold"><?php the_title(); ?></h5>
        <div class="flex-column d-flex text-center w-75 mx-auto"><?php the_excerpt($post); ?></div>
        <a href="<?php the_permalink(); ?>" class="product-button" style="background-color: #009fe3;
    color: #fff;
    font-weight: 600;
    outline: 0;
    font-size: 16px;
    border: 0;
    margin-top: 10px;
    padding: 10px 15px 10px 15px;
    border-bottom-left-radius: 15px;
    border-bottom-right-radius: 15px;
    border-top-right-radius: 15px;
    cursor: pointer;">Sprawdź</a>
        </a>

        </div>
		<?php endwhile; ?>
        </div>
        </div>
        </div>`);
    }
</script>

<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_directory'); ?>/assets/img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_directory'); ?>/assets/img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_directory'); ?>/assets/img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_directory'); ?>/assets/img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_directory'); ?>/assets/img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_directory'); ?>/assets/img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_directory'); ?>/assets/img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_directory'); ?>/assets/img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_directory'); ?>/assets/img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192" href="<?php bloginfo('template_directory'); ?>/assets/img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_directory'); ?>/assets/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_directory'); ?>/assets/img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_directory'); ?>/assets/img/favicon/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php bloginfo('template_directory'); ?>/assets/img/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-12918322-1");
        pageTracker._trackPageview();
    } catch (err) {}
</script>
<!-- Google Code for Tag remarketingowy -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 987538946;
    var google_conversion_label = "cYB0CLaM1AQQgszy1gM";
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/987538946/?value=0&amp;label=cYB0CLaM1AQQgszy1gM&amp;guid=ON&amp;script=0" />
    </div>
</noscript>