<?php


class ArrayHelper
{

    /**
     * Sortowanie tablicy ze wskazaniem pola, wg jakiego sortować
     * for value of string or integer
     *
     * @param array $array
     * @param string $keyName
     */
    static public function sortByKey(array &$array = [], string $keyName = '') {
        usort($array, self::compareByKey($keyName));
    }
    static private function compareByKey($key) {
        return function ($a, $b) use ($key) {
            return strnatcmp($a[$key], $b[$key]);
        };
    }

    /**
     * Zmiana nazwy klucza w rekordach tablicy z sortowaniem alfabetycznym wg kluczy w rekordach.
     * Użycie:
     * $array = [['staraNazwa'=>wartosc1, 'innaNazwa'=wartosc2],['staraNazwa'=>wartosc3, 'innaNazwa'=wartosc4]];
     * ArrayHepler::renameKeys($array, 'staraNazwa', 'nowaNazwa');
     * print_r($array) -> [['innaNazwa'=wartosc2, 'nowaNazwa'=>wartosc1],['innaNazwa'=wartosc4, 'nowaNazwa'=>wartosc3]];
     * @param array $array
     * @param string $oldName
     * @param string $newName
     */
    static public function renameKeys(array &$array, string $oldName, string $newName) {
        foreach($array as &$toChangeArray){
            self::renameKey($toChangeArray, $oldName, $newName);
        }
    }

    /**
     * Zmiana nazwy klucza w tablicy z sortowaniem alfabetycznym wg kluczy.
     * Użycie:
     * $array = ['staraNazwa'=>wartosc1, 'innaNazwa'=wartosc2];
     * ArrayHepler::renameKey($array, 'staraNazwa', 'nowaNazwa');
     * print_r($array) -> ['innaNazwa'=wartosc2, 'nowaNazwa'=>wartosc1];
     * @param array $array
     * @param string $oldName
     * @param string $newName
     */
    static public function renameKey(array &$array, string $oldName, string $newName) {
        if($array){
            if($oldName && $newName && array_key_exists($oldName, $array)){
                $array[$newName] = $array[$oldName];
                unset($array[$oldName]);
            }
            ksort($array);
        }
    }

    static public function clearArray(array $array) {
        foreach($array as $key=>&$value){
            if(is_array($value)){
                self::clearArray($value);
            }else{
                $value = null;
            }
        }
        return array_filter($array);
    }
}