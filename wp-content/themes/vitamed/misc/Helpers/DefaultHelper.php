<?php


class DefaultHelper
{
    /**
     * Pobieranie info o kategorii np. ilość postów.
     * Nazwa kategorii
     * @param $category_name
     * @return string
     */
    static function getCountFromCategory($category_name)
    {
        $category_id = get_cat_ID($category_name);
        $category = get_category($category_id);
        if ($category->category_count > 1 && $category->category_count < 5) {
            return $category->category_count . " dostępne preparaty";
        } elseif ($category->category_count == 1) {
            return $category->category_count . " dostępny preparat";
        } else {
            return $category->category_count . " dostępnych preparatów";
        }
    }

    static function getCountFromCategoryEnglish($category_name)
    {
        $category_id = get_cat_ID($category_name);
        $category = get_category($category_id);
        if ($category->category_count > 1 && $category->category_count < 5) {
            return $category->category_count . " available products";
        } elseif ($category->category_count == 1) {
            return $category->category_count . " available product";
        } else {
            return $category->category_count . " available products";
        }
    }

    static function tempDir()
    {
        return get_template_directory_uri();
    }

    static function imageDir($string)
    {
        return tempDir() . '/assets/img/' . $string;
    }

    static function fileDir()
    {
        return get_template_directory();
    }

    static function DayDifference($start, $end)
    {
        $start = strtotime($start);
        $end = strtotime($end);
        $difference = $end - $start;
        return round($difference / 86400);
    }


    //Nie przyznaję sie do tego
    static function isHome()
    {
        if (!is_front_page()) {
            return [
                'scrc' => $_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/logo@2.png',
                'class' => 'bg-white secondary-text language-alt link-text-alt outline',
                'scrc2' => $_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/ikona-dla-lekarzy-granatowa@2.png',
                'scrc3' => $_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/ikona-dla-farmaceutow-granatowa@2.png'
            ];
        } else {
            return [
                'scrc' => $_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/logo-biale@2.png',
                'button_preparaty' => '<button class="btn btn-primary ">Poznaj nasze preparaty</button>', 'scrc2' => $_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/ikona-dla-lekarzy-biala@2.png',
                'scrc3' => $_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/ikona-dla-farmaceutow-biala@2.png'
            ];
        }
    }

    public static function checkEn()
    {
        $r = $_SERVER['REQUEST_URI'];
        $r = explode('/', $r);
        $r = array_filter($r);
        $r = array_merge($r, array());
        $r = preg_replace('/\?.*/', '', $r);
        return $r[0];
    }
}
