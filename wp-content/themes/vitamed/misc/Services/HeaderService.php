<?php



class HeaderService
{
    private $headerHeight;

    private $headerImg;

    private $vitamedText;

    private $titleText;

    private $maskHeight;

    private $maskImg;

    private $logo;

    private $class;

    private $button;

    private $productImg;

    private $iconLekarz;

    private $iconFarmaceuta;

    public $wpQuery;

    public function __construct($wpQuery)
    {
        $this->wpQuery = $wpQuery;
        $this->getContent();
    }
    public function getTitleText()
    {
        return $this->titleText;
    }

    public function getButton()
    {
        return $this->button;
    }

    public function getButtonEnglish()
    {
        return $this->button;
    }

    public function getClass()
    {
        return $this->class;
    }

    public function getProductImg()
    {
        return $this->productImg;
    }

    public function getVitamedText()
    {
        return $this->vitamedText;
    }

    public function getHeaderHeight()
    {
        return $this->headerHeight;
    }

    public function getHeaderImg()
    {
        return $this->headerImg;
    }

    public function getLogo()
    {
        return $this->logo;
    }

    public function getMaskHeight()
    {
        return $this->maskHeight;
    }

    public function getMaskImg()
    {
        return $this->maskImg;
    }

    public function getIconLekarz()
    {
        return $this->iconLekarz;
    }

    public function getIconFarmaceuta()
    {
        return $this->iconFarmaceuta;
    }

    public function setHeaderHeight($headerHeight)
    {
        $this->headerHeight = $headerHeight;
    }

    public function setTitleText($titleText)
    {
        $this->titleText = $titleText;
    }

    public function setVitamedText($vitamedText)
    {
        $this->vitamedText = $vitamedText;
    }

    public function setHeaderImg($headerImg)
    {
        $this->headerImg = $headerImg;
    }

    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    public function setButton($button)
    {
        $this->button = $button;
    }

    public function setButtonEnglish($button)
    {
        $this->button = $button;
    }

    public function setMaskHeight($maskHeight)
    {
        $this->maskHeight = $maskHeight;
    }

    public function setMaskImg($maskImg)
    {
        $this->maskImg = $maskImg;
    }

    public function setIconLekarz($iconLekarz)
    {
        $this->iconLekarz = $iconLekarz;
    }

    public function setIconFarmaceuta($iconFarmaceuta)
    {
        $this->iconFarmaceuta = $iconFarmaceuta;
    }

    public function setProductImg($productImg)
    {
        $this->productImg = $productImg;
    }

    public function setClass($class)
    {
        $this->class = $class;
    }


    public function isHome()
    {
        if (!is_front_page()) {
            return false;
        } else {
            return true;
        }
    }


    public function getContent()
    {
        //jeśli NIE JEST stroną główną
        if (!is_front_page()) {
            if ($this->isProduct()) {
                $this->setProductImg(get_the_post_thumbnail_url('full'));
                $this->setVitamedText('<h3 class="header-produkty">PREPARATY</h3>');
                if (get_field("link")) {
                    if (DefaultHelper::checkEn() == 'en') {
                        $this->setButton('<a href="' . get_field("link") . '" target="_blank" class="header-button">Buy online</a>');
                    } else {
                        $this->setButton('<a href="' . get_field("link") . '" target="_blank" class="header-button">Kup online</a>');
                    }
                } else {
                    if (DefaultHelper::checkEn() == 'en') {
                        $this->setButton('<a href="https://ktomalek.pl" target="_blank" class="header-button">Check availability</a>');
                    } else {
                        $this->setButton('<a href="https://ktomalek.pl" target="_blank" class="header-button">Sprawdź dostępność</a>');
                    }
                }
                $this->setTitleText('<h2 class="header-title-bold-black">' . $this->wpQuery->post->post_title . '</h2>');
            } else {
                if ($_POST['slug']) {
                    $this->setTitleText('<h2 class="header-title-bold">' . $_POST['slug'] . '</h2>');
                } else {
                    $this->setTitleText('<h2 class="header-title-bold">' . $this->wpQuery->post->post_title . '</h2>');
                }
            }
            // if($this->isProductsFromCategory()){
            //     $this->setButton('<button class="header-button">Zobacz preparaty</button>');
            // }
            $this->setLogo($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/logo@2.png');
            $this->setIconLekarz($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/ikona-dla-lekarzy-granatowa@2.png');
            $this->setIconFarmaceuta($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/ikona-dla-farmaceutow-granatowa@2.png');
            $this->setHeaderHeight('70vh');
            $this->setMaskHeight('70vh');
            $this->setClass(['header' => 'header-container-scroll', 'language' => 'language-text-scroll', 'bottom-link' => 'bottom-link-scroll', 'upper-text' => 'upper-text-scroll']);
            if ($this->isProduct()) {
                $this->setHeaderImg(null);
                $this->setMaskImg(null);
            } else {
                $this->setHeaderImg($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/' . $this->checkPage());
                $this->setMaskImg($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/maskowanie-podstrona_jasny.png');
            }
        } else {
            $this->setLogo($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/logo-biale@2.png');
            $this->setIconLekarz($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/ikona-dla-lekarzy-biala@2.png');
            $this->setIconFarmaceuta($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/ikona-dla-farmaceutow-biala@2.png');
            $this->setHeaderHeight('80vh');
            $this->setMaskHeight('80vh');
            $this->setHeaderImg($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/' . $this->checkPage());
            $this->setMaskImg($_SERVER['REQUEST_URL'] . '/wp-content/themes/vitamed/assets/img/maskowanie-podstrona_jasny.png');
            $this->setTitleText('<h2 class="header-title-bold">' . mb_strtoupper($this->wpQuery->post->post_title) . '</h2>');
            if (DefaultHelper::checkEn() == 'en') {
                $this->setButton('<a href="/en/preparations/" class="header-button">Learn more about products</a>');
            } else {
                $this->setButton('<a href="/preparaty" class="header-button">Poznaj nasze preparaty</a>');
            }
        }
    }

    public function getPageName()
    {
        $post = $this->wpQuery->get_queried_object();
        return $post->post_name;
    }

    public function getPageTitle()
    {
        $post = $this->wpQuery->get_queried_object();
        return $post->post_title;
    }


    public function checkPage(): string
    {
        $pagename = $this->getPageName();
        if ($pagename == "kariera" ||  $pagename == "career") {
            return 'tlo-kariera.jpg';
        } else if ($pagename == "aktualnosci" ||  $pagename == "news") {
            return 'tlo-aktualnosci.jpg';
        } else if ($pagename == "o-nas" ||  $pagename == "about-us") {
            return 'o_nas.jpeg';
        } else if ($pagename == "pediatria" || $pagename == "dermatologia" || $pagename == "laryngologia" || $pagename == "urologia" || $pagename == "medycyna-rodzinna" || $pagename == "gastroenterologia") {
            return 'tlo-preparaty.jpeg';
        } else if ($pagename == "pediatrics" || $pagename == "dermatology" || $pagename == "laryngology" || $pagename == "urology" || $pagename == "family-medicine" || $pagename == "gastroenterology") {
            return 'tlo-preparaty.jpeg';
        } else if ($pagename == "wspolpraca" || $pagename == "cooperation") {
            return 'tlo-1.jpg';
        } else if ($pagename == "home" || $pagename == "preparaty") {
            return 'slajd-lato.jpg';
        } else if ($pagename == "home_page" || $pagename == "preparations") {
            return 'slajd-lato.jpg';
        } else if ($pagename == "kontakt" ||  $pagename == "contact") {
            return 'Kontakt.jpeg';
        } else if ($pagename == "strefa-pacjenta" ||  $pagename == "patient-area") {
            return 'strefa_pacjenta.jpg';
        } else if ($pagename == "news") {
            return 'tlo-aktualnosci.jpg';
        } else if ($pagename == "dla-niemowlat" || $pagename == "for-infants") {
            return 'Banery/matka.jpg';
        } else if ($pagename == "dla-calej-rodziny") {
            return 'Banery/rodzina.jpg';
        } else if ($pagename == "okres-jesienno-zimowy") {
            return 'Banery/jesien.jpg';
        } else if ($pagename == "zasady-zywienia-dietetycznego-w-kamicy-nerkowej") {
            return 'Banery/kamica.jpg';
        } else if ($pagename == "nie-wiesz-jak-poradzic-sobie-z-chrapaniem") {
            return 'Banery/chrapanie.jpg';
        } else if ($pagename == "refluks-u-noworodkow-i-dzieci" || $pagename == "what-is-it-and-how-to-relieve-the-symptoms-of-gastroesophageal-reflux-in-newborns-and-children") {
            return 'Banery/dziecko.jpg';
        } else {
            return 'tlo-aktualnosci.jpg';
        }
    }

    public function isProduct()
    {
        if (get_post_meta(get_the_ID(), '_wp_page_template', TRUE) == 'single-product.php') {
            return true;
        } else {
            return false;
        }
    }

    public function isProductsFromCategory()
    {
        $pagename = $this->getPageName();
        if ($pagename == "pediatria" || $pagename == "dermatologia" || $pagename == "laryngologia" || $pagename == "urologia" || $pagename == "medycyna-rodzinna" || $pagename == "gastroenterologia") {
            return true;
        } else {
            return false;
        }
    }

    public function requireCarousel()
    {
        $pagename = $this->getPageName();
        if ($pagename == "home" || $pagename == "home_page") {
            return true;
        } else {
            return false;
        }
    }
}
