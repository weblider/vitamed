<?php


class ProductsService
{


    public static function getByCategory($offset = null, $posts_per_page = -1, $category_name = null){
        $args = array(
            'post_type'=> 'post',
            'category_name' => $category_name,
            'orderby'    => 'date',
            'post_status' => 'publish',
            'order'    => 'DESC',
            'offset' => $offset,
            'posts_per_page' => $posts_per_page,
            'language' => 'pl'
        );
        $result = new WP_Query( $args );
        return $result;
    }

    public static function getProducts($offset = null, $posts_per_page = -1, $type = 'Produkty'){
        return self::getByCategory($offset,$posts_per_page, $type);
    }

    public static function getNews($offset = null, $posts_per_page = -1){
        return self::getByCategory($offset,$posts_per_page, 'Aktualnosci');
    }

	public static function getStrefa($offset = null, $posts_per_page = -1){
		return self::getByCategory($offset,$posts_per_page, 'Strefa pacjenta');
	}

	public static function getWpisyStrefa($name){
		return self::getPostsByTemplate($name);
	}

    public static function getPagesByTemplate($value){
        return get_pages(array(
	        'sort_order'   => 'ASC',
	        'sort_column'  => 'post_date',
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-templates/'.$value
        ));
    }

	public static function getPostsByTemplate($value){
		return get_posts(array(

			'meta_key' => '_wp_page_template',
			'meta_value' => 'page-templates/'.$value
		));
	}

}