<?php

global $post;
class Supports
{

    function __construct()
    {
        add_action( 'after_setup_theme', 'custom_header' );
        add_action( 'init', 'the_post_thumbnail' );
        add_theme_support('post-thumbnails');
        add_post_type_support( 'page', 'excerpt' );
        add_filter( 'wpcf7_autop_or_not', '__return_false' );
        $this->custom_header();
    }

    public function custom_header()
    {
        $args = array(
            'default-image'          => get_template_directory_uri().'/assets/img/slajd-lato.jpg',
            'random-default'         => false,
            'width'                  => 960,
            'height'                 => 100,
            'flex-height'            => true,
            'flex-width'             => true,
            'header-text'            => true,
            'default-text-color'     => '',
            'uploads'                => true,
            'wp-head-callback'      => 'wphead_cb',
            'admin-head-callback'       => 'adminhead_cb',
            'admin-preview-callback'    => 'adminpreview_cb',
        );
        add_theme_support( 'custom-header', $args );
    }

    public function checkPage(){

    }

}