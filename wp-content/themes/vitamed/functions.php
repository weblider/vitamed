<?php

//Linki i funkcje admina
require 'misc/Helpers/DefaultHelper.php';


//LHeader
require 'misc/Services/HeaderService.php';

//Tworzenie nowego menu, operacje na menu
require 'misc/Helpers/MenuHelper.php';

//Walidacja stringów, manipulacja
require 'misc/Helpers/StringHelper.php';

//Załączanie JSów i cssów
require 'misc/Helpers/LinksHelper.php';

//Zarządzanie produktami
require 'misc/Services/ProductsService.php';

//Linki i funkcje admina
require 'misc/Services/AdminService.php';

//Potrzebne linkowanie akcji, theme_support i filtrów
require 'misc/Supports.php';



add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);
add_action('init', 'the_post_thumbnail');
add_theme_support('post-thumbnails');
function special_nav_class($classes, $item)
{
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}

function count_cat_post($category)
{
    if (is_string($category)) {
        $catID = get_cat_ID($category);
    } elseif (is_numeric($category)) {
        $catID = $category;
    } else {
        return 0;
    }
    $cat = get_category($catID);
    return $cat->count;
}

add_theme_support('menus');

//limit przycięcia funkcji excerpt
function custom_excerpt_length($length)
{
    return 15;
}
add_filter('excerpt_length', 'custom_excerpt_length', 999);

//link czytaj wiecej funcji excerpt
function new_excerpt_more($more)
{
    return ' <a href="' . get_permalink(get_the_ID()) . '">Czytaj dalej >></a>';
}


function get_relative_permalink($url)
{
    $url = get_permalink();
    return str_replace(home_url(), "", $url);
}
