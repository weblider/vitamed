<?php
get_header();

?>

<?php if ( have_posts() ) : ?>
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/news.css"/>

<main role="main" class="w-100">
    <div class="container ">
        <h1 class="page-title text-center mb-3 pb-3 pt-3">Wyniki wyszukiwania: </h1>
        <div class="row news-list">
            <?php while ( have_posts() ) : the_post(); ?>
                <div class="col " style="">
                    <a style="text-decoration: none; color: black" href="<?php the_permalink();?>">
                        <div class="card  d-table-cell align-middle h-100" style="width: 18rem;">
                            <div class="position-relative text-center">
                                <img class="card-img-top" src="<?php the_post_thumbnail_url( 'medium' ); ?>" alt="Card image cap">
                                <div class="data"><?php echo get_the_date( 'd.m.Y' ); ?></div>
                            </div>
                            <div class="card-body d-flex flex-column">
                                <h5 class="card-title"><?php the_title(); ?></h5>
                                <p class="card-text mt-auto"><?php the_excerpt($post); ?></p>
                            </div>
                        </div>
                    </a>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</main>


    <?php /* Start the Loop */ ?>



<?php else : ?>

    <?php get_template_part( 'no-results', 'search' ); ?>

<?php endif; ?>

<?php get_footer();?>
