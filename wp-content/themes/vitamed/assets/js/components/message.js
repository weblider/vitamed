/**
 * @author arkadiusz.dobrzanski@weblider.eu
 */

export {showMessage, hideMessage}

function showMessage(message, classMessage='error', toBlock='main > .content')
{
    console.log('showMessage void');
    $(toBlock).prepend(
        '<div class="jq-message ' + classMessage +'">' + message + '<div class="message-close" onclick="hideMessage();">×</div></div>'
    );

    $('.jq-message.' + classMessage).show(300).delay(15000).hide(400);
    console.log('showMessage ready');
}

function hideMessage()
{
    $(this).parent('.jq-message').hide(300);
}

$(document).ready(function () {
    console.log('message OK');
});
