/**
 * @author arkadiusz.dobrzanski@weblider.eu
 */

export {liveLoadData, filterLoad, dataFromForm, submitData, getData, submitDataFile}
import {showMessage} from './message';
import {strToArray} from './helpers';

/**
 *
 * STARY SUMBITDATA,TRZEBA GO ZMIENIć!!!!!!!!!!!
 */
function submitData(data = null,method, searchValue, url, fill)
{
    let request = $.ajax({
      //uderzam do customowego routingu
        url: url + "/" + searchValue,
        type: method,
        data: data,
        fail: function () {},
        success: function (result) {
          //w data jest zwracany cały wygląd HTML wraz z wprowadzonymi danymi
            console.log(searchValue);
            $(fill).hide().fadeOut(250).html(result).fadeIn(350);
          //Więc wrzucam 'data' do tej klasy
        },
    });
}



// renderowanie htmla
function liveLoadData($formObj, $fillObj)
{
//  console.log('livedata/liveLoadData/formObject', $formObj);
    var action = $formObj.attr('action');
    console.log('liveLoadData::action', action);
    submitData(dataFromForm($formObj), action, $fillObj);
}

function filterLoad($formObj, $fillObj)
{
    var action = $fillObj.data('action');
    console.log('filterLoad::action', action);
    submitData(dataFromForm($formObj), action, $fillObj);
}

function dataFromForm($formObj, asJson = 1)
{
    if (!$formObj.is('form')) {
        var formData = $formObj.closest('form').serializeArray();
    } else {
        var formData = $formObj.serializeArray();
    }
    console.log('dataFromForm::formData', formData);

    var data = {};
    if (jQuery.isArray(formData)) {
        $(formData).each(function (index, obj) {
            var nameRaw = obj.name,
            name = nameRaw.replace('[]', '[' + index + ']');
            data[name] = obj.value;
        });
    }
    return asJson ? JSON.stringify(data) : data;
}

// wysyłanie zapytania do API ale bez plików
function submitDataNew(data, action, fill = null, typeFill = 'html', $formFind = null, $fillFind = null)
{
    console.log('submitData POST', {data: data, action: action, fill: fill, typeFill: typeFill});
    $.post(action, data, function (response) {
      // return
        try {
          // response is json
            var o = jQuery.parseJSON(response);
            console.log('RESPONSE JSON', o);
        } catch (error) {
            var o = response;
            console.log('RESPONSE NoJSON', o);
        }

        console.log('fill', fill);
        if (fill === 'redirect') {
            console.log('o.success', o.success);
            console.log('o.url', o.url);
            if (o.success !== undefined && o.success && o.url !== undefined) {
                window.location.href = o.url;
            }
        }

      // zwracany przez kontroler php content, komunikaty i opcjonalnie fill oraz typeFill
        var content = '';
        var f = fill;
        var tf = typeFill;
        if (typeof o === 'string' || typeof o === 'number') {
            content = o; // string
        } else {
          // obiekt musi zawierać klucz o nazwie content z zawartością
            if (o.hasOwnProperty('content')) {
                content = o.content;
            }
            if (o.hasOwnProperty('fill')) {
                f = o.fill;
            }
            if (o.hasOwnProperty('typeFill')) {
                tf = o.typeFill;
            }
          // wiadomości
            if (o.hasOwnProperty('message') && o.message) {
                showMessage(o.message, 'info');
            }
            if (o.hasOwnProperty('alert') && o.alert) {
                showMessage(o.alert, 'alert');
            }
            if (o.hasOwnProperty('error') && o.error) {
                showMessage(o.error, 'error');
            }
        }
      // dodanie odpowiedzi do obiektu
        var $ob = {};
        if (f !== null) {
            if (f instanceof jQuery) {
                $ob = f;
            } else {
                $ob = $(f);
            }
            if (tf === 'val' || tf === 'value') {
                $ob.val(content);
            } else if (tf === 'number' || tf === 'text' || tf === 'txt') {
                $ob.text(content);
            } else if (tf === 'html') {
                $ob.html(content);
            } else if (tf === 'show') {
                $ob.show(700);
            } else if (tf === 'hide') {
                $ob.hide(700);
            } else {
                showMessage('Data "typeFill" ' + tf + ' is unknown type!', 'error', 'main');
            }
        }
        if ($formFind !== null && $fillFind !== null) {
            liveLoadData($formFind, $fillFind);
        }
    });
}

function submitDataFile($form, action, fill = null, typeFill = 'html')
{
//  var form = $("#myform")[0];

    $.ajax({
        url: action,
        data: new FormData($form),
        type: "post",
        contentType: false,
        processData: false,
        cache: false,
        dataType: "json",
        error: function (err) {
            console.error(err);
        },
        success: function (data) {
            console.log(data);

            try {
              // response is json
                var o = jQuery.parseJSON(data);
                console.log('RESPONSE JSON', o);
            } catch (error) {
                var o = data;
                console.log('RESPONSE NoJSON', o);
            }
          // zwracany przez kontroler php content, komunikaty i opcjonalnie fill oraz typeFill
            var content = '';
            var f = fill;
            var tf = typeFill;
            if (typeof o === 'string' || typeof o === 'number') {
                content = o; // string
            } else {
              // obiekt musi zawierać klucz o nazwie content z zawartością
                if (o.hasOwnProperty('content')) {
                    content = o.content;
                }
                if (o.hasOwnProperty('fill')) {
                    f = o.fill;
                }
                if (o.hasOwnProperty('typeFill')) {
                    tf = o.typeFill;
                }
              // messages
                if (o.hasOwnProperty('message') && o.message) {
                    showMessage(o.message, 'info');
                }
                if (o.hasOwnProperty('alert') && o.alert) {
                    showMessage(o.alert, 'alert');
                }
                if (o.hasOwnProperty('error') && o.error) {
                    showMessage(o.error, 'error');
                }
            }
          // dodanie odpowiedzi do obiektu
            var $ob = {};
            if (f !== null) {
                if (f instanceof jQuery) {
                    $ob = f;
                } else {
                    $ob = $(f);
                }
                if (tf === 'val' || tf === 'value') {
                    $ob.val(content);
                } else if (tf === 'number' || tf === 'text' || tf === 'txt') {
                    $ob.text(content);
                } else if (tf === 'html') {
                    $ob.html(content);
                } else if (tf === 'show') {
                    $ob.show(700);
                } else if (tf === 'hide') {
                    $ob.hide(700);
                } else {
                    showMessage('Data "typeFill" ' + tf + ' is unknown type!', 'error', 'main');
                }
            }

        },
        complete: function () {
            console.log("Request upload finished.");
        }
    });
}


// the same as axios for vue, but only for data - without sending files
function getData(action, fill = null, typeFill = 'html', $formFind = null, $fillFind = null)
{
    console.log('submitData GET', {action: action, fill: fill, typeFill: typeFill});
    $.get(action, function (response) {
      // return
        try {
          // response is json
            var o = jQuery.parseJSON(response);
            console.log('RESPONSE JSON', o);
            if (o === true && $formFind !== null && $fillFind !== null) {
                // jeśli odpowiedź dla get jest dokładnie "true" i podano formularz + miejsce zwrotu dla niego
                liveLoadData($formFind, $fillFind);
                return false;
            }
        } catch (error) {
            var o = response;
            console.log('RESPONSE NoJSON', o);
        }
      // zwracany przez kontroler php content, komunikaty i opcjonalnie fill oraz typeFill
        var content = '';
        var f = fill;
        var tf = typeFill;
        if (typeof o === 'string' || typeof o === 'number') {
            content = o; // string
        } else {
          // obiekt musi zawierać klucz o nazwie content z zawartością
            if (o.hasOwnProperty('content')) {
                content = o.content;
            }
            if (o.hasOwnProperty('fill')) {
                f = o.fill;
            }
            if (o.hasOwnProperty('typeFill')) {
                tf = o.typeFill;
            }
          // messages
            if (o.hasOwnProperty('message') && o.message) {
                showMessage(o.message, 'info');
            }
            if (o.hasOwnProperty('alert') && o.alert) {
                showMessage(o.alert, 'alert');
            }
            if (o.hasOwnProperty('error') && o.error) {
                showMessage(o.error, 'error');
            }
        }
      // dodanie odpowiedzi do obiektu
        var $ob = {};
        if (f !== null) {
            if (f instanceof jQuery) {
                $ob = f;
            } else {
                $ob = $(f);
            }
            if (tf === 'val' || tf === 'value') {
                $ob.val(content);
            } else if (tf === 'number' || tf === 'text' || tf === 'txt') {
                $ob.text(content);
            } else if (tf === 'html') {
                $ob.html(content);
            } else if (tf === 'show') {
                $ob.show(700);
            } else if (tf === 'hide') {
                $ob.hide(700);
            } else {
                showMessage('Data "typeFill" ' + tf + ' is unknown type!', 'error', 'main');
            }
        } else {
            return content;
        }
    });
}


