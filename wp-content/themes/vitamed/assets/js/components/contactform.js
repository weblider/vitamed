/**
 * @author arkadiusz.dobrzanski@weblider.eu
 */


import {submitData} from './ajax';
import {showMessage} from "./message";

$(document).ready(function () {

    $("#sendMail").on("click", function () {
        console.log("Contact form sending...");
        var formObj = $(this).data("formId");
        var formData = $(formObj).closest('form').serializeArray();
        let method = "POST";
        let id = $(this).data("id");
        let fill = "#alert";
        let classMessage = $(this).data("classMessage");
        let url = `/wp-json/contact-form-7/v1/contact-forms/${id}/feedback`;
        let searchValue = null;
        console.log("formObject: " + formObj);
        console.log("formData: " + formData);
        showMessage($(this).html(), classMessage, $(this));
        submitData(formData,method, searchValue, url, fill);
    });

});

