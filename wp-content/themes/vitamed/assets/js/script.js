const menu = document.querySelector(".header-container");
const header = document.querySelector(".header.relative");
const headerTitleContainer = document.querySelector(".header-title-container");
const image = document.querySelector(".header-logo");
const headerImage = document.querySelector(".header-image");
const docImage = document.querySelector(".doctor");
const pharImage = document.querySelector(".pharmacist");
const linkText = document.querySelectorAll(".bottom-link");
const text = document.querySelectorAll(".upper-text");
const language = document.querySelectorAll(".language-text");
const languageItem = document.querySelectorAll(".language-item");
const border = document.querySelector(".current-menu-item");
const patient = document.querySelectorAll(".patient-link");
const activeLinkBorder = document.querySelector(".active-link");
const searchBox = document.querySelector(".search-input-text");
const pageTitle = document.querySelector(".header-title-bold");
const mask = document.querySelector(".header-mask");

let vW = $(window).width();
let vH = $(window).height();




$(window)
  .on("mousemove", function () {
    let mW = ((event.pageX * 100) / vW).toFixed(3) - 50;
    let mH = ((event.pageY * 100) / vH).toFixed(3) - 50;

    $(".polaris-image").css(
      "transform",
      "translateX(" + +mW / 4 + "%) translateY(" + +mH / 6 + "%)"
    );
  })
  .on("mouseleave", function () {
    $(".polaris-image").css("transform", "translateX(0), translateY(0)");
  });

$(window)
  .on("mousemove", function () {
    let mW = ((event.pageX * 100) / vW).toFixed(3) - 50;
    let mH = ((event.pageY * 100) / vH).toFixed(3) - 50;

    $(".solaris-image").css(
      "transform",
      "translateX(" + +mW / -3 + "%) translateY(" + +mH / -5 + "%)"
    );
  })
  .on("mouseleave", function () {
    $(".solaris-image").css("transform", "translateX(0), translateY(0)");
  });

// $(document).on("mousemove", function (e) {
//   $(".polaris-image").animate(
//     {
//       right: e.pageX + 2 + "px",
//       top: e.pageY + 2 + "px",
//     },
//     0
//   );
// });

jQuery(function ($) {
  var path = window.location.href;
  $("li a").each(function () {
    if (this.href === path) {
      if (this.text == "Start") {
        $(this).parent().addClass("active-link");
      }
    }
  });
});
// searchBox.addEventListener('keyup', (e) => {
//   let inputValue = e.target.value;
//   localStorage.setItem("searchValue", inputValue);

//   const sessionElement = localStorage.getItem("searchValue");

//   if (window.location.pathname.includes("/wyszukiwarka/")) {
//     pageTitle.textContent = sessionElement
//   }
// })

// function singleHandler() {
//   if (window.location.pathname.includes("/aktualnosci/")) {
//     const col4 = document.querySelector('.col-4');
//     const col8 = document.querySelector('.col-8');
//     const singleContainer = document.querySelector('.single-container');

//     col8.style.maxWidth = "66.66667%";
//   }
// }
//
// // singleHandler();
// const cookieBar = document.querySelector(".cookie-policy-bar");
//
// function cookieBarHandler() {
//   const cookieBarButton = document.querySelector(".cookie-policy-button");
//   if (window.location.pathname.includes("/en")) {
//     $(".cookie-policy-text").text(
//       "By using our site, you consent to the use of cookies"
//     );
//     $(".cookie-policy-link").text("More information in Privacy Policy");
//     $(".cookie-policy-link").attr("href", "/en/privacy-policy");
//     $(".cookie-policy-button").text("Accept");
//   }
//
//   cookieBarButton.addEventListener("click", () => {
//     localStorage.setItem("showCookie", "false");
//     cookieBar.style.display = "none";
//   });
//
//   if (localStorage.getItem("showCookie") == "false") {
//     cookieBar.style.display = "none";
//   }
// }
//
// cookieBarHandler();

if (window.location.pathname.includes("/en")) {
  const getTiles = document.querySelectorAll(".border-spec");

  getTiles.forEach((element) => {
    element.classList.remove("border-spec");
    element.classList.add("border-spec-eng");
  });
}

function testFunkcji() {
  if (window.location.pathname.includes("/wyszukiwarka")) {
    console.log(window.location.search.split("=")[1]);
    pageTitle.textContent = window.location.search.split("=")[1];
  }

  // document.querySelector(".header-title-bold").text = "asd";
  // console.log()
}

testFunkcji();

function imageInHeader() {}

if (window.location.pathname.includes("/produkty")) {
  imageInHeader();
}

function patientMobileMenu() {
  $(".patient-mobile").click(function (e) {
    e.preventDefault();
    $(".patient-container-mobile").toggle("active");
  });
}

function headerPositionHandler() {
  if (
    window.location.pathname.includes("/wspolpraca") ||
    window.location.pathname.includes("/cooperation")
  ) {
    headerImage.classList.add("header-wspolpraca");
  }

  if (
    window.location.pathname.includes("/aktualnosci") ||
    window.location.pathname.includes("/news")
  ) {
    headerImage.classList.add("header-aktualnosci");
    mask.style.height = "70vh";
  }

  if (window.location.pathname == "/") {
    const naglowek = document.querySelector(".naglowek-napis");
    headerImage.classList.add("header-glowna");
    naglowek.classList.add("mobilny-naglowek");
    headerTitleContainer.style.justifyContent = "space-around";
    headerTitleContainer.classList.remove("container");
  }

  if (
    window.location.pathname.includes("/o-nas") ||
    window.location.pathname.includes("/about-us")
  ) {
    headerImage.classList.add("header-o-nas");
    header.classList.add("header-main-o-nas");
    mask.classList.add("header-mask-o-nas");
  }
  if (window.location.pathname.includes("/kategoria")) {
    headerImage.classList.add("header-preparaty");
  }

  if (
    window.location.pathname.includes("/pediatria") ||
    window.location.pathname.includes("/dermatologia") ||
    window.location.pathname.includes("/laryngologia") ||
    window.location.pathname.includes("/urologia") ||
    window.location.pathname.includes("/medycyna-rodzinna") ||
    window.location.pathname.includes("/gastroenterologia")
  ) {
    headerImage.classList.add("header-aktualnosci");
  }

  if (
    window.location.pathname.includes("/pediatrics") ||
    window.location.pathname.includes("/dermatology") ||
    window.location.pathname.includes("/laryngology") ||
    window.location.pathname.includes("/urology") ||
    window.location.pathname.includes("/family-medicine") ||
    window.location.pathname.includes("/gastroenterology")
  ) {
    headerImage.classList.add("header-aktualnosci");
  }

  if (
    window.location.pathname.includes("/preparaty") ||
    window.location.pathname.includes("/preparations")
  ) {
    headerImage.classList.add("header-preparaty");
  }
  if (
    window.location.pathname.includes("/strefa-pacjenta") ||
    window.location.pathname.includes("/patient-area")
  ) {
    headerImage.classList.add("header-strefa-pacjenta");
    mask.style.height = "70vh";
  }
  if (
    window.location.pathname.includes("/kariera") ||
    window.location.pathname.includes("/career")
  ) {
    headerImage.classList.add("header-kariera");
  }
  if (
    window.location.pathname.includes("/kontakt") ||
    window.location.pathname.includes("/contact")
  ) {
    headerImage.classList.add("header-kontakt");
  }
}

// function patientMenu() {
//   // $(".patient").mouseover(function (e) {
//   //   e.preventDefault();
//   //   $(".patient-container").addClass("active");
//   //   $(".header-container").addClass("header-container-expand");

//     // $(".header-container").mouseleave(function (e) {
//     //   e.preventDefault();
//     //   $(".header-container").removeClass("header-container-expand");
//     // });
//   });

//   // $(".patient-container").mouseleave(function (e) {
//   //   e.preventDefault();
//   //   $(".patient-container").removeClass("active");
//   //   $(".header-container").removeClass("header-container-expand");
//   // });

//   // $(".patient-container").mouse(function (e) {
//   //   e.preventDefault();
//   //   console.log("yupikayey");
//   //   $(".patient-container").removeClass("active");
//   // });
// }

// function singleHandler() {
//   if (window.location.pathname.includes("/strefa-pacjenta/") && window.location.pathname.length > 13) {
//     changeBackground();
//   }
// }

// singleHandler();

if (window.location.pathname == "/") {
  // const carousel = document.querySelector('.mobile-carousel');
  if (window.innerWidth < 764) {
    // carousel.setAttribute("data-flickity", `{"imagesLoaded":true, "arrowShape": "M 12.881 47.156 L 41.738 20.164 L 48.024 26.125 L 32.959 41.407 L 84.983 41.407 L 84.983 52.571 L 32.923 52.636 L 47.184 65.096 L 42.005 72.977 Z" ,"autoPlay": 4500, "groupCells": 1, "prevNextButtons": true, "pageDots": false, "freeScroll":true, "contain":false, "wrapAround":true }`);
    image.src =
      window.location.origin +
      "/wp-content/themes/vitamed/assets/img/logo@2.png";
  } else {
    // carousel.setAttribute("data-flickity", `{"imagesLoaded":true, "arrowShape": "M 12.881 47.156 L 41.738 20.164 L 48.024 26.125 L 32.959 41.407 L 84.983 41.407 L 84.983 52.571 L 32.923 52.636 L 47.184 65.096 L 42.005 72.977 Z" ,"autoPlay": 4500, "groupCells": 2, "prevNextButtons": true, "pageDots": false, "freeScroll":true, "contain":false, "wrapAround":true }`);
    if (window.devicePixelRatio == 1.5) {
      // carousel.setAttribute("data-flickity", `{"imagesLoaded":true, "arrowShape": "M 12.881 47.156 L 41.738 20.164 L 48.024 26.125 L 32.959 41.407 L 84.983 41.407 L 84.983 52.571 L 32.923 52.636 L 47.184 65.096 L 42.005 72.977 Z" ,"autoPlay": 4500, "groupCells": 1, "prevNextButtons": true, "pageDots": false, "freeScroll":true, "contain":false, "wrapAround":true }`);
    }
    image.src =
      window.location.origin +
      "/wp-content/themes/vitamed/assets/img/logo-biale@2.png";
  }
}

function changeBackground() {
  const headerTitle = document.querySelector(".header-title-bold");

  const headerImage = document.querySelector(".header-image");
  const headerRelative = document.querySelector(".header-relative");

  mask.style.backgroundImage =
    "url(/wp-content/themes/vitamed/assets/img/maskowanie-podstrona_jasny.png);";
  headerImage.style.backgroundImage =
    "url(/wp-content/themes/vitamed/assets/img/tlo-preparaty.jpeg)";
  headerTitle.style.color = "#fff";
  mask.style.height = "70vh";
}

function isMobile() {
  window.addEventListener("resize", () => {
    if (window.innerWidth < 1024) {
      image.src =
        window.location.origin +
        "/wp-content/themes/vitamed/assets/img/logo@2.png";
    } else {
      image.src =
        window.location.origin +
        "/wp-content/themes/vitamed/assets/img/logo-biale@2.png";
    }
  });
}

function patientAreaHandler(site_url) {
  if (site_url.includes("dla-niemowlat") || site_url.includes("for-infants")) {
    headerImage.classList.add("dla-niemowlat");
  }

  if (site_url.includes("dla-calej-rodziny")) {
    headerImage.classList.add("dla-calej-rodziny");
  }

  if (site_url.includes("okres-jesienno-zimowy")) {
    headerImage.classList.add("okres-jesienno-zimowy");
  }

  if (site_url.includes("zasady-zywienia-dietetycznego-w-kamicy-nerkowej")) {
    headerImage.classList.add("kamica-nerkowa");
  }

  if (site_url.includes("nie-wiesz-jak-poradzic-sobie-z-chrapaniem")) {
    headerImage.classList.add("chrapanie");
  }

  if (site_url.includes("refluks-u-noworodkow-i-dzieci")) {
    headerImage.classList.add("refluks");
  }
}

function backgroundHandler() {
  if (
    window.location.pathname.includes("/aktualnosci/") &&
    window.location.pathname.length > 13
  ) {
    changeBackground();
  }

  if (
    window.location.pathname.includes("/strefa-pacjenta/") ||
    (window.location.pathname.includes("/patient-zone/") &&
      window.location.pathname.length > 17)
  ) {
    patientAreaHandler(window.location.pathname);
  }

  if (
    window.location.pathname.includes("/en/news/") &&
    window.location.pathname.length > 8
  ) {
    changeBackground();
  }
}

function hamburgerMenu() {
  // let mobileIcon = document.querySelector('.mobile-icon');
  // let mobileList = document.querySelector('.mobile-block');
  $(".mobile-icon").click(function (e) {
    e.preventDefault();
    $(".mobile-block").toggleClass("active");
  });

  // mobileIcon.addEventListener('click', () => {

  // })
}

function languageMenuHandler() {
  if (window.location.pathname.includes("/en/")) {
    document.querySelector(".english").classList.add("active-language");
  } else {
    document.querySelector(".polish").classList.add("active-language");
  }
}

function searchInput() {
  // let mobileIcon = document.querySelector('.mobile-icon');
  // let mobileList = document.querySelector('.mobile-block');
  $(".search-icon").click((e) => {
    e.preventDefault();
    $(".search-input").toggle();
    $(".header-container").toggleClass("header-container-expand");

    // $(".mobile-block").toggleClass("active");
  });

  // mobileIcon.addEventListener('click', () => {

  // })
}

backgroundHandler();
hamburgerMenu();
searchInput();
isMobile();
headerPositionHandler();
// patientMenu();
patientMobileMenu();
languageMenuHandler();

// function load_main() {
//   window.onload = () => {
//     image.src = menu.querySelector('.img-responsive').src;
//     // docImage.src = window.location.origin + "/wp-content/themes/vitamed/assets/img/ikona-dla-lekarzy-biala@2.png";
//     // pharImage.src = window.location.origin +  "/wp-content/themes/vitamed/assets/img/ikona-dla-farmaceutow-biala@2.png";
//     menu.classList.remove('secondary-header');
//     menu.classList.remove('shadow');
//     border.children[0].classList.remove('outline-alt');
//     for (let i = 0; i < language.length; i++) {
//       language[i].classList.remove('language-alt');
//     }
//     for (let i = 0; i < linkText.length; i++) {
//       linkText[i].classList.remove('link-text-alt');
//     }
//     for (let i = 0; i < text.length; i++) {
//       text[i].classList.remove('secondary-text');
//     }
//   }
// }

// load_main();

document.addEventListener("scroll", () => {
  let scrollTop = document.documentElement.scrollTop || document.body.scrollTop;

  // if (window.location.pathname == "/") {
  //   menu.classList.add("shadow");
  // }

  //Działa, ale można poprawić
  if (window.location.pathname == "/" && window.innerWidth > 1024) {
    if (scrollTop > 300) {
      image.src =
        window.location.origin +
        "/wp-content/themes/vitamed/assets/img/logo@2.png";
      docImage.src =
        window.location.origin +
        "/wp-content/themes/vitamed/assets/img/ikona-dla-lekarzy-granatowa@2.png";
      pharImage.src =
        window.location.origin +
        "/wp-content/themes/vitamed/assets/img/ikona-dla-farmaceutow-granatowa@2.png";

      if (
        document.querySelector(".bottom-item").classList.contains("active-link")
      ) {
        document
          .querySelector(".bottom-item")
          .classList.add("active-link-blue");
      }

      if (
        document.querySelector(".english").classList.contains("active-language")
      ) {
        document.querySelector(".english").classList.add("active-link-blue");
      }

      if (
        document.querySelector(".polish").classList.contains("active-language")
      ) {
        document.querySelector(".polish").classList.add("active-link-blue");
      }

      // menu.classList.add("secondary-header");
      menu.classList.add("header-container-scroll");
      // border.children[0].classList.add("outline-alt");
      for (let i = 0; i < patient.length; i++) {
        patient[i].classList.add("patient-link-scroll");
      }
      for (let i = 0; i < language.length; i++) {
        language[i].classList.add("language-text-scroll");
      }
      for (let i = 0; i < linkText.length; i++) {
        linkText[i].classList.add("bottom-link-scroll");
      }
      for (let i = 0; i < text.length; i++) {
        text[i].classList.add("upper-text-scroll");
      }
    } else {
      image.src =
        window.location.origin +
        "/wp-content/themes/vitamed/assets/img/logo-biale@2.png";
      if (window.location.pathname == "/") {
        docImage.src =
          window.location.origin +
          "/wp-content/themes/vitamed/assets/img/ikona-dla-lekarzy-biala@2.png";
        pharImage.src =
          window.location.origin +
          "/wp-content/themes/vitamed/assets/img/ikona-dla-farmaceutow-biala@2.png";
        // border.children[0].classList.remove("outline-alt");

        if (
          document
            .querySelector(".bottom-item")
            .classList.contains("active-link-blue")
        ) {
          document
            .querySelector(".bottom-item")
            .classList.remove("active-link-blue");
        }

        if (
          document
            .querySelector(".english")
            .classList.contains("active-link-blue")
        ) {
          document
            .querySelector(".english")
            .classList.remove("active-link-blue");
        }

        if (
          document
            .querySelector(".polish")
            .classList.contains("active-link-blue")
        ) {
          document
            .querySelector(".polish")
            .classList.remove("active-link-blue");
        }
      }
      // menu.classList.remove("secondary-header");
      menu.classList.remove("header-container-scroll");
      for (let i = 0; i < patient.length; i++) {
        patient[i].classList.remove("patient-link-scroll");
      }

      for (let i = 0; i < language.length; i++) {
        language[i].classList.remove("language-text-scroll");
      }
      for (let i = 0; i < linkText.length; i++) {
        linkText[i].classList.remove("bottom-link-scroll");
      }
      for (let i = 0; i < text.length; i++) {
        text[i].classList.remove("upper-text-scroll");
      }
    }
  } else {
    $(function () {
      image.src =
        window.location.origin +
        "/wp-content/themes/vitamed/assets/img/logo@2.png";
      docImage.src =
        window.location.origin +
        "/wp-content/themes/vitamed/assets/img/ikona-dla-lekarzy-granatowa@2.png";
      pharImage.src =
        window.location.origin +
        "/wp-content/themes/vitamed/assets/img/ikona-dla-farmaceutow-granatowa@2.png";
      menu.classList.add("secondary-header");

      if (
        document.querySelector(".bottom-item").classList.contains("active-link")
      ) {
        document
          .querySelector(".bottom-item")
          .classList.add("active-link-blue");
      }

      if (
        document.querySelector(".english").classList.contains("active-language")
      ) {
        document.querySelector(".english").classList.add("active-link-blue");
      }

      if (
        document.querySelector(".polish").classList.contains("active-language")
      ) {
        document.querySelector(".polish").classList.add("active-link-blue");
      }

      menu.classList.add("header-container-scroll");
      // border.children[0].classList.add("outline-alt");
      for (let i = 0; i < patient.length; i++) {
        patient[i].classList.add("patient-link-scroll");
      }
      for (let i = 0; i < language.length; i++) {
        language[i].classList.add("language-text-scroll");
      }
      for (let i = 0; i < linkText.length; i++) {
        linkText[i].classList.add("bottom-link-scroll");
      }
      for (let i = 0; i < text.length; i++) {
        text[i].classList.add("upper-text-scroll");
      }
    });
  }

  // document.querySelector("meta[name=viewport]").setAttribute('content', 'width=device-width, user-scalable=no, initial-scale='+(1/window.devicePixelRatio));

  // hamburgerMenu();

  // $(document).ready(function () {

  //   $('.search-icon').click(function () {
  //     if ($(".search").css("display") == "none") {
  //       if ($(window).width() < 960) {
  //         $(".search-icon").hide();
  //         $(".img-responsive").attr("src", window.location.origin + "/wp-content/themes/vitamed/assets/img/logo-biale@2.png");
  //         $(".search").css({ 'margin-top': '33px' });
  //         $(".search").css({ 'margin-right': '80%' });

  //       }
  //       $('.search').animate(
  //         {
  //           width: '0px'
  //         }, 100, function () {
  //           $(this).hide();
  //           $('.search').animate(
  //             {
  //               width: '250px'
  //             }, 1000, function () {
  //               $(this).show();
  //             }
  //           );
  //         }
  //       );
  //     } else {

  //     }
  //   });

  // });
});
